## docker

This directory should contain all docker related stuff.

`Dockerfile-node` is an exception because according to [Heroku's docs](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml):

> The Docker build context is always set to the directory containing the Dockerfile and cannot be configured independently.

# Installation

You can start the required services by running

```bash
# Start node service
$ docker-compose up node
```

# Deploying Image to Heroku

Build and push Docker image to heroku:

```bash
$ docker build -f Dockerfile-node
$ docker tag <image> registry.heroku.com/<app>/web
$ docker push registry.heroku.com/<app>/web
```

Create a new release:

```bash
$ heroku container:release web
```

For more info related to Heroku containers visit https://devcenter.heroku.com/articles/container-registry-and-runtime.
