import { createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";
import seeds from "../typeorm/seeds";

async function run() {
  const connection = await createConnection(ormconfig);

  // tslint:disable-next-line:array-type
  const queries: Promise<any>[] = seeds
    .map(seed => {
      const repo = connection.getRepository(seed.entity);
      // @ts-ignore
      return seed.seeds.map(value => repo.save(value));
    })
    .reduce((a, b) => a.concat(b), []); // flatten array of promises [[RoleA, RoleB], [GenreA, GenreB]]

  await Promise.all(queries);

  process.exit(0);
}

run();
