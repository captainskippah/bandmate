import { Musician } from "@bandmate/types/musician";
import { generateRelativeLocation } from "../tests/utils";

if (!process.env.TEST_LOCATION_CENTER) {
  console.error("Environment variable `TEST_LOCATION_CENTER` not set");
  process.exit(1);
}

const baseURL =
  process.env.NODE_ENV === "local"
    ? "http://192.168.0.11:3000"
    : "https://bandmate-io.herokuapp.com";

const [latitude, longitude] = process.env.TEST_LOCATION_CENTER!.split(",");

type MusicianSeed = Musician & { name: string; image: string };

const facebookSeeds: MusicianSeed[] = [
  {
    id: "193cfe0b-b819-4b31-bc7c-8e22da709e1a", // this is local (not facebook or firebase) ID
    name: "Chika Fujiwara",
    image: `${baseURL}/profile4.jpg`,
    bio: "I am a vocalist that likes blues",
    genres: ["blues"],
    roles: ["vocalist"],
    location: generateRelativeLocation(Number(latitude), Number(longitude), 10)
  },
  {
    id: "3554c6b5-8dd8-4ee1-a87c-0ee5a922a814", // this is local (not facebook or firebase) ID
    name: "Megumin",
    image: `${baseURL}/profile3.jpg`,
    bio: "I am a guitarist that likes rock",
    genres: ["rock"],
    roles: ["guitarist"],
    location: generateRelativeLocation(Number(latitude), Number(longitude), 15)
  },
  {
    id: "04c84582-6c9c-4cb5-bd58-70038642c600", // this is local (not facebook or firebase) ID
    name: "Rem",
    image: `${baseURL}/profile2.png`,
    bio: "I am a drummer that likes jazz",
    genres: ["jazz"],
    roles: ["drummer"],
    location: generateRelativeLocation(Number(latitude), Number(longitude), 25)
  }
];

export default facebookSeeds;
