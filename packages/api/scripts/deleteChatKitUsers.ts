import { default as Chatkit } from "@pusher/chatkit-server";
import * as inquirer from "inquirer";

async function run() {
  const { answer } = await inquirer.prompt([
    {
      type: "confirm",
      name: "answer",
      message: "Are you sure you want to delete all Chatkit users?",
      default: false
    }
  ]);

  if (!answer) {
    console.log("Exiting...");
    process.exit(0);
  }

  const chatkit = new Chatkit({
    instanceLocator: process.env.CHATKIT_LOCATOR_ID!,
    key: process.env.CHATKIT_SECRET!
  });

  const users: [{ id: string }] = await chatkit.getUsers();

  try {
    await Promise.all(
      users.filter(user => user.id !== "admin").map(user => chatkit.deleteUser({ userId: user.id }))
    );

    console.log("Successfully deleted all Chatkit users.");
    process.exit(0);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

run();
