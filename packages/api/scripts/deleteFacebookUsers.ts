import axios from "axios";
import * as firebaseAdmin from "firebase-admin";
import * as inquirer from "inquirer";
import { createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";
import TypeOrmMusicianRepository from "../src/infrastructure/repositories/TypeOrmMusicianRepository";

async function deleteFacebookUser(fbId: string): Promise<void> {
  console.log(`Deleting Facebook id "${fbId}"`);
  const endpoint = `https://graph.facebook.com/v4.0/${fbId}/?access_token=${process.env.FB_APP_ID}|${process.env.FB_APP_SECRET}`;
  await axios.delete(endpoint);
}

async function run() {
  const { answer } = await inquirer.prompt([
    {
      type: "confirm",
      name: "answer",
      message: "Are you sure you want to delete all Facebook users?",
      default: false
    }
  ]);

  if (!answer) {
    console.log("Exiting...");
    process.exit(0);
  }

  const connection = await createConnection(ormconfig);

  interface Seed {
    id: string;
    musician: string;
  }

  const existingSeeds: Seed[] = await connection.query("SELECT id, musician FROM seeds");

  if (existingSeeds.length === 0) {
    console.log("No User seeds exists. No actions are done.");
    process.exit(0);
  }

  const firebaseApp = firebaseAdmin.initializeApp(require("../firebase-client"));
  const musicianRepo = new TypeOrmMusicianRepository(connection);

  async function deleteFirebaseUser(id: string): Promise<void> {
    console.log(`Deleting Firebase id "${id}"`);
    await firebaseApp.auth().deleteUser(id);
  }

  async function deleteMusician(id: string) {
    console.log(`Deleting Musician "${id}"`);
    await musicianRepo.delete(id);
  }

  Promise.all(
    existingSeeds.map(async seed => {
      const firebaseUser = await firebaseApp.auth().getUser(seed.musician);
      await deleteFacebookUser(firebaseUser.providerData[0].uid);
      await deleteFirebaseUser(firebaseUser.uid);
      await deleteMusician(firebaseUser.uid);
      connection.query(`DELETE FROM seeds WHERE id = '${seed.id}'`);
    })
  ).then(() => {
    console.log(`Deleted ${existingSeeds.length} users`);
    console.log("deleteFBUsers has finished!");
    process.exit(0);
  });
}

run();
