import { default as Chatkit } from "@pusher/chatkit-server";
import * as firebaseAdmin from "firebase-admin";

async function run() {
  const firebaseApp = firebaseAdmin.initializeApp();

  const chatkit = new Chatkit({
    instanceLocator: process.env.CHATKIT_LOCATOR_ID!,
    key: process.env.CHATKIT_SECRET!
  });

  const firebaseUsers = await firebaseApp.auth().listUsers();

  // @ts-ignore
  let chatKitUsers = (await chatkit.getUsers()).map((user: Chatkit) => user.id);

  chatKitUsers = firebaseUsers.users
    .filter(user => !chatKitUsers.includes(user.uid))
    .map(user => ({
      id: user.uid,
      name: user.displayName!,
      avatarURL: user.photoURL!
    }));

  try {
    await chatkit.createUsers({ users: chatKitUsers });
    process.exit(0);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

run();
