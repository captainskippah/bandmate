import { Genre, Musician, Role } from "@bandmate/types/musician";
import * as faker from "faker";
import * as firebaseAdmin from "firebase-admin";
import { createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";
import TypeOrmMusicianRepository from "../src/infrastructure/repositories/TypeOrmMusicianRepository";
import { generateRelativeLocation } from "../tests/utils";

if (!process.env.TEST_LOCATION_CENTER) {
  console.error("Environment variable `TEST_LOCATION_CENTER` not set");
  process.exit(1);
}

const [latitude, longitude] = process.env.TEST_LOCATION_CENTER!.split(",");

type RollbackFunc = () => Promise<void>;

// From https://stackoverflow.com/a/2450976/10975709
function shuffle(array: string[]) {
  let currentIndex = array.length;
  let temporaryValue;
  let randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

const ROLES: Role[] = ["bassist", "drummer", "guitarist", "keyboardist", "vocalist"];
const GENRES: Genre[] = ["blues", "jazz", "metal", "pop", "reggae", "rock"];

async function run() {
  const connection = await createConnection(ormconfig);
  const musicianRepo = new TypeOrmMusicianRepository(connection);

  const firebaseApp = firebaseAdmin.initializeApp();

  const rollback: RollbackFunc[] = [];

  // tslint:disable-next-line:prefer-array-literal
  const requests: Array<Promise<void>> = [];

  // tslint:disable-next-line:no-increment-decrement
  for (let i = 0; i < 15; i++) {
    requests.push(
      (async () => {
        const firebaseUser = await firebaseApp.auth().createUser({
          email: faker.internet.email(),
          password: "superStrongPassword!",
          emailVerified: true,
          disabled: false,
          displayName: faker.name.findName(),
          photoURL: `https://api.adorable.io/avatars/64/${faker.internet.userName()}`
        });

        rollback.push(() => firebaseApp.auth().deleteUser(firebaseUser.uid));

        const roleCount = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
        const genreCount = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
        const distance = Math.floor(Math.random() * (40 - 15 + 1)) + 15;

        shuffle(ROLES);
        shuffle(GENRES);

        const musician: Musician = {
          id: firebaseUser.uid,
          name: firebaseUser.displayName!,
          image: firebaseUser.photoURL!,
          bio: faker.lorem.sentence(),
          roles: ROLES.slice(0, roleCount),
          genres: GENRES.slice(0, genreCount),
          location: generateRelativeLocation(Number(latitude), Number(longitude), distance)
        };

        await musicianRepo.save(musician);

        rollback.push(() => musicianRepo.delete(musician.id));
      })()
    );
  }

  Promise.all(requests)
    .then(() => {
      console.log("createFirebaseUsers has finished!");
      process.exit(0);
    })
    .catch(async e => {
      console.error(e);
      await Promise.all(rollback.map(rollbackFn => rollbackFn()));
    });
}

run();
