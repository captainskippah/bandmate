import { createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";

async function run() {
  const connection = await createConnection({
    ...ormconfig,
    migrationsRun: true
  });

  await connection.runMigrations({ transaction: true });

  process.exit(0);
}

run()
