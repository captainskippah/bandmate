import { Musician } from "@bandmate/types/musician";
import axios, { AxiosResponse } from "axios";
import * as firebase from "firebase";
import "firebase/auth";
import { createConnection, Table } from "typeorm";
import ormconfig from "../typeorm/ormconfig";
import TypeOrmMusicianRepository from "../src/infrastructure/repositories/TypeOrmMusicianRepository";
import facebookSeeds from "./facebookSeeds";

interface FacebookCreateUserRequest {
  installed?: boolean;
  minor?: boolean;
  name?: string;
}

interface FacebookCreateUserResponse {
  id: string;
  access_token: string;
  login_url: string;
  email: string;
  password: string;
}

type RollbackFunc = () => Promise<void>;

async function deleteFacebookUser(fbId: string): Promise<void> {
  console.log(`Deleting Facebook id "${fbId}"`);
  const endpoint = `https://graph.facebook.com/v4.0/${fbId}/?access_token=${process.env.FB_APP_ID}|${process.env.FB_APP_SECRET}`;
  await axios.delete(endpoint);
}

async function createFacebookUser(name: string): Promise<FacebookCreateUserResponse> {
  console.log(`Creating Facebook user "${name}"`);
  const endpoint = `https://graph.facebook.com/v4.0/${process.env.FB_APP_ID}/accounts?access_token=${process.env.FB_APP_ID}|${process.env.FB_APP_SECRET}`;
  const response = await axios.post<
    FacebookCreateUserRequest,
    AxiosResponse<FacebookCreateUserResponse>
  >(endpoint, { name });
  console.log("Facebook user created", response.data);
  return response.data;
}

async function run() {
  const connection = await createConnection(ormconfig);

  const table = new Table({
    name: "facebook_seeds",
    columns: [
      { name: "id", type: "uuid", isPrimary: true },
      { name: "musician", type: "varchar", isPrimary: true }
    ]
  });

  await connection.createQueryRunner().createTable(table, true);

  const seedIds = facebookSeeds.map(seed => seed.id);
  const existingSeeds = await connection.query("SELECT id FROM facebook_seeds");

  if (existingSeeds.length === 3) {
    console.log("User seeds exists. No actions are made.");
    process.exit(0);
    return;
  }

  // Remove recipients that already exists in our DB
  const newSeeds = facebookSeeds.filter(seed => seedIds.includes(seed.id));
  const firebaseApp = firebase.initializeApp(require("../firebase-client"));
  const musicianRepo = new TypeOrmMusicianRepository(connection);

  async function deleteMusician(id: string) {
    console.log(`Deleting Musician "${id}"`);
    await musicianRepo.delete(id);
  }

  async function createMusician(musician: Musician) {
    console.log("Creating Musician record", musician);
    await musicianRepo.save(musician);
    console.log("Musician created");
    return musician.id;
  }

  async function deleteFirebaseUser(firebaseUser: firebase.User) {
    console.log(`Deleting Firebase user "${firebaseUser.uid}"`);
    await firebaseUser.delete();
  }

  async function createFirebaseUser(
    facebookUser: FacebookCreateUserResponse
  ): Promise<firebase.User> {
    const { access_token, id } = facebookUser;
    console.log(`Creating firebase user for facebook token "${access_token}"`);
    const fbCredential = firebase.auth.FacebookAuthProvider.credential(access_token);
    const response = await firebaseApp.auth().signInWithCredential(fbCredential);
    return response.user!;
  }

  await Promise.all(
    newSeeds.map(async seed => {
      const rollback: RollbackFunc[] = [];

      try {
        const fbUser = await createFacebookUser(seed.name);
        rollback.push(() => deleteFacebookUser(fbUser.id));

        const firebaseUser = await createFirebaseUser(fbUser);
        rollback.push(() => deleteFirebaseUser(firebaseUser));

        await firebaseUser.updateProfile({ photoURL: seed.image });
        console.log("Updated firebase user `photoURL`");

        const musicianId = await createMusician({ ...seed, id: firebaseUser.uid });
        rollback.push(() => deleteMusician(musicianId));

        await connection.query(
          `INSERT INTO facebook_seeds (id, musician) VALUES ('${seed.id}', '${musicianId}')`
        );

        return musicianId;
      } catch (e) {
        console.error(e);
        await Promise.all(rollback.map(rollbackFn => rollbackFn()));
      }
    })
  ).then(ids => {
    console.log(`IDs created: ${ids.join(",")}`);
    console.log("createFacebookUsers has finished!");
    process.exit(0);
  });
}

run();
