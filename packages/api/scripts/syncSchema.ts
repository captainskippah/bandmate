import { createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";

async function run() {
  const connection = await createConnection(ormconfig);

  await connection.synchronize(false);

  process.exit(0);
}

run()
