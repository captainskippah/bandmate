import { default as Chatkit } from "@pusher/chatkit-server";

const chatkit = new Chatkit({
  instanceLocator: process.env.CHATKIT_LOCATOR_ID!,
  key: process.env.CHATKIT_SECRET!
});

async function run() {
  try {
    await chatkit.createUser({
      id: "admin",
      name: "Admin",
      avatarURL: "http://192.168.0.11:3000/admin.png"
    });
  } catch (e) {
    if (e.error !== "services/chatkit/user_already_exists") {
      throw e;
    }
  }

  process.exit(0);
}

run()
