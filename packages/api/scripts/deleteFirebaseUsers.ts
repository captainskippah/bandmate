import * as firebaseAdmin from "firebase-admin";
import * as inquirer from "inquirer";
import { createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";
import TypeOrmMusicianRepository from "../src/infrastructure/repositories/TypeOrmMusicianRepository";

async function run() {
  const { answer } = await inquirer.prompt([
    {
      type: "confirm",
      name: "answer",
      message: "Are you sure you want to delete all Firebase users?",
      default: false
    }
  ]);

  if (!answer) {
    console.log("Exiting...");
    process.exit(0);
  }

  const firebaseApp = firebaseAdmin.initializeApp();

  const connection = await createConnection(ormconfig);
  const musicianRepo = new TypeOrmMusicianRepository(connection);

  const { users } = await firebaseApp.auth().listUsers();

  try {
    await Promise.all(
      users.map(async user => {
        await firebaseApp.auth().deleteUser(user.uid);
        await musicianRepo.delete(user.uid);
      })
    );

    console.log("Successfully deleted all Firebase users.");

    process.exit(0);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

run();
