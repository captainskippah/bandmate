# BandMate - api

A Tinder-like app for musicians who are looking for others they can collaborate with.

## Environment Variables

Set the following required Environment Variables:

- `DATABASE_URL` - The url for the database that is going to be used
- `GOOGLE_APPLICATION_CREDENTIALS` - Service account key path location. This is downloaded from Firebase console.
- `FIREBASE_CONFIG` - Firebase config file path location. This is downloaded from Firebase console.
- `CHATKIT_LOCATOR_ID` - Chatkit credentials. This can be found at Chatkit's dashboard.
- `CHATKIT_SECRET` - Chatkit credentials. This can be found at Chatkit's dashboard.

Other Environment Variables that is used by some scripts:
 
- `FB_APP_ID` - Facebook App Id to be used by facebook-related scripts.
- `FB_APP_SECRET` - Facebook App Secret to be used by facebook-related scripts.
- `TEST_LOCATION_CENTER` - Developer's current coordinates used for generating fake users within this location.

## Build Setup

`docker-compose up node` should be used but in case you need to build it manually do the following steps.

**Important**: You still need to run migrations and seed database on docker setup by running the required scripts with `docker-compose exec node` . See [Running Scripts](#running-scripts) for more info on how to run scripts.

``` bash
# install dependencies
$ yarn install

# build  files
$ yarn run build

# run migrations
$ yarn ts-node -r dotenv/config scripts/runMigration.ts

# seed database
$ yarn ts-node -r dotenv/config scripts/seedDatabase.ts

# launch server
$ yarn start
```

## Running Scripts

Type `yarn ts-node -r dotenv/config scripts/<name of script>.ts` to run the desired script.

Scripts can be found inside `scripts/` directory.
