import { Musician } from "@bandmate/types/musician";
import firebaseAdmin from "firebase-admin";
import { inject, injectable } from "inversify";
import { Connection, EntityManager } from "typeorm";
import uuid from "uuid";
import { musicianEntity, MusicianSchema } from "../../../typeorm/schemas";
import MusicianRepository from "../../domain/musician/MusicianRepository";

@injectable()
export default class TypeOrmMusicianRepository implements MusicianRepository {
  private readonly manager: EntityManager;

  constructor(@inject("Connection") connection: Connection) {
    this.manager = connection.manager;
  }

  public async getById(id: string): Promise<Musician | undefined> {
    try {
      const firebaseUser = await firebaseAdmin.auth().getUser(id);

      const musician = await this.manager.findOne(
        musicianEntity,
        { id },
        {
          relations: ["roles", "genres"],
          loadRelationIds: true
        }
      );

      if (musician === undefined) {
        return undefined;
      }

      // @ts-ignore
      return {
        ...musician,
        name: firebaseUser.displayName!,
        image: firebaseUser.photoURL!
      };
    } catch (e) {
      if (e.code === "auth/user-not-found") {
        return undefined;
      }
      throw e;
    }
  }

  public async nextIdentity(): Promise<string> {
    return uuid.v4();
  }

  public async save(musician: Musician): Promise<void> {
    const { bio, roles, genres, ...rest } = musician;
    const repo = this.manager.getRepository(musicianEntity);

    const musicianSchema: MusicianSchema = {
      ...rest,
      bio: bio.trim(),
      genres: (genres || []).map(genre => ({ id: genre })),
      roles: (roles || []).map(role => ({ id: role }))
    };

    const existing =
      (await repo
        .createQueryBuilder()
        .where("id = :id", { id: musician.id })
        .getCount()) === 1;

    await repo.save(musicianSchema);

    if (existing) {
      // We execute this last so if we ever had a TypeORM related error
      // at least we didn't consumed firebase which might get hit again
      // and again when user experienced a problem.
      await firebaseAdmin.auth().updateUser(musician.id, {
        displayName: musician.name,
        photoURL: musician.image
      });
    }
  }

  public async delete(id: string): Promise<void> {
    await this.manager
      .createQueryBuilder()
      .delete()
      .from("musician")
      .where("id = :id", { id })
      .execute();
  }
}
