import { inject, injectable } from "inversify";
import { Connection } from "typeorm";

@injectable()
export default class UserDeviceService {
  private readonly connection: Connection;

  constructor(@inject("Connection") connection: Connection) {
    this.connection = connection;
  }

  public async registerDevice(userId: string, deviceToken: string) {
    await this.connection
      .createQueryBuilder()
      .insert()
      .into("user_device")
      .values([{ musician: userId, token: deviceToken }])
      .execute();
  }

  public async getDevices(userId: string): Promise<[{ token: string }]> {
    return await this.connection.query("SELECT token FROM user_device WHERE musician = $1", [
      userId
    ]);
  }
}
