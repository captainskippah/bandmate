import { CollabFilter } from "@bandmate/types/collab";
import { Musician } from "@bandmate/types/musician";
import firebaseAdmin from "firebase-admin";
import { inject, injectable } from "inversify";
import { Connection, Repository } from "typeorm";

@injectable()
export default class CollabQueryService {
  private readonly repository: Repository<Musician>;

  constructor(@inject("Connection") connection: Connection) {
    this.repository = connection.getRepository("Musician");
  }

  public async getMusicians(finderId: string, filter?: CollabFilter): Promise<Musician[]> {
    const qb = this.repository
      .createQueryBuilder("m")
      .addSelect("ST_Distance(m.location, m2.location, false) AS dist")
      .loadAllRelationIds()
      .leftJoin("musician_role", "mr", "m.id = mr.musician")
      .leftJoin("musician_genre", "mg", "m.id = mg.musician")
      .leftJoin("musician", "m2", "m2.id = :finderId", { finderId })
      .orderBy("dist", "ASC")
      .andWhere("m.id != :finderId");

    qb.andWhere("ST_DWithin(m.location, m2.location, :distance * 1000, false)", {
      distance: filter && filter.maxDistance ? filter.maxDistance : 10
    });

    if (filter && filter.genre && filter.genre.trim()) {
      qb.andWhere("mg.genre = :genre", { genre: filter.genre });
    }

    if (filter && filter.role && filter.role.trim()) {
      qb.andWhere("mr.role = :role", { role: filter.role });
    }

    qb.setParameter("finderId", finderId);

    let musicians = await qb.getMany();

    const initialPromise: Promise<Musician[]> = Promise.resolve([]);

    musicians = await musicians.reduce(async (promise, musician) => {
      try {
        const user = await firebaseAdmin.auth().getUser(musician.id);
        const array = await promise;

        array.push({
          ...musician,
          name: user.displayName!,
          image: user.photoURL!
        });

        return array;
      } catch (e) {
        console.error("Firebase error. Excluding musician from results.");
        console.error(e);
        return promise;
      }
    }, initialPromise);

    return musicians;
  }
}
