import { Location, Musician } from "@bandmate/types/musician";
import { default as Chatkit } from "@pusher/chatkit-server";
import { inject, injectable } from "inversify";
import { ParameterizedContext } from "koa";
import MusicianRepository from "src/domain/musician/MusicianRepository";
import UserDeviceService from "../application/UserDeviceService";

@injectable()
export default class MusicianController {
  private readonly musicianRepository: MusicianRepository;
  private readonly chatkit: Chatkit;
  private readonly userDeviceService: UserDeviceService;

  constructor(
    @inject("MusicianRepository") musicianRepository: MusicianRepository,
    @inject("Chatkit") chatkit: Chatkit,
    @inject("UserDeviceService") userDeviceService: UserDeviceService
  ) {
    this.musicianRepository = musicianRepository;
    this.chatkit = chatkit;
    this.userDeviceService = userDeviceService;
  }

  public async getById(context: ParameterizedContext) {
    let id = context.params.id;

    if (id === "current") {
      id = context.state.user.uid;
    }

    const musician = await this.musicianRepository.getById(id);

    if (musician === undefined) {
      context.throw(404);
    } else {
      context.response.body = musician;
    }
  }

  public async updateOrCreate(context: ParameterizedContext) {
    try {
      let id = context.params.id;
      const musician: Omit<Musician, "id"> = context.request.body;

      if (id === "current") {
        id = context.state.user.uid;
      }

      await this.musicianRepository.save({ ...musician, id });

      await this.chatkit
        .getUser({ id })
        .then(user => {
          this.chatkit.updateUser({
            id,
            name: musician.name,
            avatarURL: musician.image
          });
        })
        .catch(async e => {
          console.error(e);
          await this.chatkit.createUser({
            id,
            name: musician.name,
            avatarURL: musician.image
          });
        });

      context.response.status = 204;
    } catch (e) {
      console.error(e);
      if (e.error !== undefined) {
        context.response.status = e.status;
      } else {
        context.response.status = 500;
      }
    }
  }

  public async updateLocation(context: ParameterizedContext) {
    let id = context.params.id;
    const location: Location = context.request.body;

    if (id === "current") {
      id = context.state.user.uid;
    }

    const musician = await this.musicianRepository.getById(id);

    if (musician === undefined) {
      context.response.status = 404;
    } else {
      musician.location = location;

      await this.musicianRepository.save(musician);

      context.response.status = 204;
    }
  }

  public async registerDevice(context: ParameterizedContext) {
    let id = context.params.id;
    const token = context.request.body.token;

    if (id === "current") {
      id = context.state.user.uid;
    }

    const musician = await this.musicianRepository.getById(id);

    if (musician === undefined) {
      context.response.status = 404;
    } else {
      const devices = await this.userDeviceService.getDevices(id);

      if (!devices.some(device => device.token === token)) {
        await this.userDeviceService.registerDevice(id, token);
      }

      context.response.status = 204;
    }
  }
}
