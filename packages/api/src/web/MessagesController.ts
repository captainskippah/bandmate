import { default as Chatkit } from "@pusher/chatkit-server";
import firebaseAdmin from "firebase-admin";
import { inject, injectable } from "inversify";
import { ParameterizedContext } from "koa";
import UserDeviceService from "../application/UserDeviceService";

@injectable()
export default class MessagesController {
  private readonly chatkit: Chatkit;
  private readonly userDeviceService: UserDeviceService;

  constructor(
    @inject("Chatkit") chatkit: Chatkit,
    @inject("UserDeviceService") userDeviceService: UserDeviceService
  ) {
    this.chatkit = chatkit;
    this.userDeviceService = userDeviceService;
  }

  public async sendMessage(context: ParameterizedContext) {
    const roomId = context.params.roomId;

    try {
      const sendMessageResponse = await this.chatkit.sendSimpleMessage({
        roomId,
        userId: context.state.user.uid,
        text: context.request.body.text
      });

      const room = await this.chatkit.getRoom({
        roomId
      });

      const recipientId =
        room.member_user_ids[0] === context.state.user.uid
          ? room.member_user_ids[1]
          : room.member_user_ids[0];

      const recipientDevices = await this.userDeviceService.getDevices(recipientId);
      const tokens = recipientDevices.map(device => device.token);

      await firebaseAdmin
        .messaging()
        .sendMulticast({
          tokens,
          notification: {
            title: context.state.user.name,
            body: context.request.body.text,
            imageUrl: context.state.user.picture
          },
          data: {
            roomId
          }
        })
        .then(response => {
          if (response.failureCount > 0) {
            response.responses
              .filter(sendResponse => !sendResponse.success)
              .forEach(sendResponse => console.warn(sendResponse.error));
          }
        })
        .catch(e => {
          console.warn("Unable to send notifications to firebase messaging.", e);
        });

      context.response.status = 201;
      context.response.body = sendMessageResponse;
    } catch (e) {
      console.error(e);
      context.response.status = 500;
    }
  }
}
