import { inject, injectable } from "inversify";
import { ParameterizedContext } from "koa";
import CollabQueryService from "../application/collab/CollabQueryService";

@injectable()
export default class CollabController {
  private readonly collabQueryService: CollabQueryService;

  constructor(@inject("CollabQueryService") collabQueryService: CollabQueryService) {
    this.collabQueryService = collabQueryService;
  }

  public async findMusicians(context: ParameterizedContext) {
    const { maxDistance, role, genre } = context.request.query;
    const musicians = await this.collabQueryService.getMusicians(context.state.user.uid, {
      maxDistance,
      role,
      genre
    });

    context.status = 200;
    context.body = musicians;
  }
}
