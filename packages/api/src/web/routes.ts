import { Container } from "inversify";
import { default as Router } from "koa-router";
import authenticated from "./authenticated";
import CollabController from "./CollabController";
import MessagesController from "./MessagesController";
import MusicianController from "./MusicianController";

/**
 * @param container - Container used for resolving Controllers
 */
export default function createRoutes(container: Container): Router {
  const router = new Router();

  const authenticatedRouter = new Router();
  authenticatedRouter.use(authenticated);

  const musicianController = container.resolve(MusicianController);

  authenticatedRouter.get("/musicians/:id", musicianController.getById.bind(musicianController));

  authenticatedRouter.put(
    "/musicians/:id",
    musicianController.updateOrCreate.bind(musicianController)
  );

  authenticatedRouter.put(
    "/musicians/:id/location",
    musicianController.updateLocation.bind(musicianController)
  );

  authenticatedRouter.post(
    "/musicians/:id/devices",
    musicianController.registerDevice.bind(musicianController)
  );

  const collabController = container.resolve(CollabController);

  authenticatedRouter.get("/collab", collabController.findMusicians.bind(collabController));

  const messagesController = container.resolve(MessagesController);

  authenticatedRouter.post(
    "/rooms/:roomId/messages",
    messagesController.sendMessage.bind(messagesController)
  );

  router.use(authenticatedRouter.routes(), authenticatedRouter.allowedMethods());

  return router;
}
