import { default as firebase } from "firebase-admin";
import { Middleware } from "koa";

const authenticated: Middleware<firebase.auth.DecodedIdToken> = async (context, next) => {
  const authorization = context.request.get("authorization");

  const idToken = authorization.startsWith("Bearer")
    ? authorization.slice(7, authorization.length).trim()
    : "";

  try {
    const decodedToken = await firebase.auth().verifyIdToken(idToken);

    if (decodedToken) {
      context.state.user = decodedToken;
      return next();
    }

    context.throw(401);
  } catch (e) {
    console.error(e);
    if (e.code !== undefined) {
      context.throw(401);
    } else {
      context.throw(500);
    }
  }
};

export default authenticated;
