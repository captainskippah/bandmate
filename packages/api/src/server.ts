import { Connection, createConnection } from "typeorm";
import ormconfig from "../typeorm/ormconfig";
import { default as createApplication } from "./app";
import { default as createContainer } from "./container";
import MusicianRepository from "./domain/musician/MusicianRepository";
import TypeOrmMusicianRepository from "./infrastructure/repositories/TypeOrmMusicianRepository";
import { default as createRouter } from "./web/routes";

if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
  console.error("Environment variable `GOOGLE_APPLICATION_CREDENTIALS` not set");
  process.exit(1);
}

if (!process.env.FIREBASE_CONFIG) {
  console.error("Environment variable `FIREBASE_CONFIG` not set");
  process.exit(1);
}

if (!process.env.DATABASE_URL) {
  console.error("Environment variable `DATABASE_URL` not set");
  process.exit(1);
}

if (!process.env.CHATKIT_LOCATOR_ID) {
  console.error("Environment variable `CHATKIT_LOCATOR_ID` not set");
  process.exit(1);
}

if (!process.env.CHATKIT_SECRET) {
  console.error("Environment variable `CHATKIT_SECRET` not set");
  process.exit(1);
}

async function start() {
  const dbConnection = await createConnection(ormconfig);

  const container = createContainer([
    _container => _container.bind<Connection>("Connection").toConstantValue(dbConnection),
    _container =>
      _container.bind<MusicianRepository>("MusicianRepository").to(TypeOrmMusicianRepository)
  ]);

  const router = await createRouter(container);

  const app = await createApplication(container, router);

  const port = process.env.PORT || 3000;

  app.listen(port, () => {
    console.log("Available Routes:");

    router.stack.forEach(stack => {
      stack.methods.forEach(method => {
        console.log(
          `${method}: ${stack.path} -> ${stack.stack.map(middleware => middleware.name).join(",")}`
        );
      });
    });

    console.log(`Listening on port ${port}`);
  });
}

start();
