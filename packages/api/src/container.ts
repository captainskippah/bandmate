import { default as Chatkit } from "@pusher/chatkit-server";
import { Container } from "inversify";
import CollabQueryService from "./application/collab/CollabQueryService";
import UserDeviceService from "./application/UserDeviceService";

type Binding = (container: Container) => void;

const defaultBindings: Binding[] = [
  container => container.bind("CollabQueryService").to(CollabQueryService),
  container => container.bind("UserDeviceService").to(UserDeviceService),
  container =>
    container.bind("Chatkit").toConstantValue(
      new Chatkit({
        instanceLocator: process.env.CHATKIT_LOCATOR_ID!,
        key: process.env.CHATKIT_SECRET!
      })
    )
];

export default function createContainer(bindings: Binding[] = []): Container {
  const container = new Container();

  defaultBindings.forEach(binding => binding(container));
  bindings.forEach(binding => binding(container));

  return container;
}
