import pgConnectionString from "pg-connection-string";

const pgConnection = pgConnectionString.parse(process.env.DATABASE_URL!);

export const database = {
  host: pgConnection.host || "localhost",
  port: Number(pgConnection.port) || 5432,
  username: pgConnection.user!,
  password: pgConnection.password || undefined,
  database: pgConnection.database!
};
