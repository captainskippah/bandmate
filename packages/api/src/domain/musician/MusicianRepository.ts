import { Musician } from "@bandmate/types/musician";

interface MusicianRepository {
  getById(id: string): Promise<Musician | undefined>;
  nextIdentity(): Promise<string>;
  save(musician: Musician): Promise<void>;

  // this one is currently used for scripts related to generating recipients only.
  delete(id: string): Promise<void>;
}

export default MusicianRepository;
