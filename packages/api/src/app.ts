import firebaseAdmin from "firebase-admin";
import { Container } from "inversify";
import { default as Koa } from "koa";
import { default as bodyParser } from "koa-bodyparser";
import { default as Router } from "koa-router";
import "reflect-metadata";

async function createApplication(container: Container, router: Router): Promise<Koa> {
  firebaseAdmin.initializeApp();

  const app = new Koa();

  app
    .use(bodyParser())
    .use(router.routes())
    .use(router.allowedMethods());

  return app;
}

export default createApplication;
