import { Location } from "@bandmate/types/musician";
import { Point } from "geojson";

export function toRadians(deg: number): number {
  return (deg * Math.PI) / 180;
}

export function toDegrees(rad: number): number {
  return (rad * 180) / Math.PI;
}

export function toGeoJSON(location: Location): Point {
  return {
    type: "Point",
    coordinates: [location.longitude, location.latitude]
  };
}
