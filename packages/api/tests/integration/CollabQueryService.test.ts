import { Connection, createConnection } from "typeorm";
import * as uuid from "uuid";
import CollabQueryService from "../../src/application/collab/CollabQueryService";
import { database } from "../../src/config";
import { musicianEntity } from "../../typeorm/schemas";
import { generateRelativeLocation } from "../utils";

describe("CollabQueryService", () => {
  let connection: Connection;
  let collabQueryService: CollabQueryService;

  beforeAll(async () => {
    connection = await createConnection({
      type: "postgres",
      host: database.host,
      port: database.port,
      username: database.username,
      password: database.password,
      database: "bandmate_testing",
      synchronize: true,
      logging: true,
      entities: [musicianEntity]
    });

    collabQueryService = new CollabQueryService(connection);
  });

  afterAll(async () => {
    await connection.close();
  });

  beforeEach(async () => {
    await connection.query("BEGIN");
  });

  afterEach(async () => {
    await connection.query("ROLLBACK");
  });

  it("should search Musicians within 10km, for any role and genre by default", async () => {
    // Get from device GPS
    const userLocation = { latitude: 15.143262, longitude: 120.583843 };

    await connection
      .createQueryBuilder()
      .insert()
      .into(musicianEntity)
      .values([
        {
          id: uuid.v4(),
          genres: ["metal"],
          roles: ["guitarist"],
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 5)
        },
        {
          id: uuid.v4(),
          genres: ["blues"],
          roles: ["drummer"],
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 10)
        },
        {
          id: uuid.v4(),
          genres: ["jazz"],
          roles: ["bassist"],
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 15)
        }
      ])
      .execute();

    const matches = await collabQueryService.getMusicians(userLocation);

    expect(matches).toHaveLength(2);
    expect(matches.some(match => match.name === "marcy")).toBeFalsy();
  });

  it("can search within given maxDistance", async () => {
    // Get from device GPS
    const userLocation = new Location(15.143262, 120.583843);

    await connection
      .createQueryBuilder()
      .insert()
      .into(musicianEntity)
      .values([
        {
          id: new MusicianId(faker.random.uuid()),
          name: "finn",
          genre: "metal",
          role: "guitarist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 15)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "jake",
          genre: "blues",
          role: "drummer",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 5)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "marcy",
          genre: "jazz",
          role: "bassist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 25)
        }
      ])
      .execute();

    const matches = await collabQueryService.getMusicians(userLocation, { maxDistance: 5 });

    expect(matches).toHaveLength(1);
    expect(matches[0].name === "jake").toBeTruthy();
  });

  it("can search for given role", async () => {
    // Get from device GPS
    const userLocation = new Location(15.143262, 120.583843);

    await connection
      .createQueryBuilder()
      .insert()
      .into(musicianEntity)
      .values([
        {
          id: new MusicianId(faker.random.uuid()),
          name: "finn",
          genre: "metal",
          role: "guitarist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 5)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "jake",
          genre: "blues",
          role: "drummer",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 10)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "marcy",
          genre: "jazz",
          role: "bassist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 15)
        }
      ])
      .execute();

    const matches = await collabQueryService.getMusicians(userLocation, { role: "guitarist" });

    expect(matches).toHaveLength(1);
    expect(matches[0].name === "finn").toBeTruthy();
  });

  it("can search for given genre", async () => {
    // Get from device GPS
    const userLocation = new Location(15.143262, 120.583843);

    await connection
      .createQueryBuilder()
      .insert()
      .into(musicianEntity)
      .values([
        {
          id: new MusicianId(faker.random.uuid()),
          name: "finn",
          genre: "metal",
          role: "guitarist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 5)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "jake",
          genre: "blues",
          role: "drummer",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 10)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "marcy",
          genre: "jazz",
          role: "bassist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 10)
        }
      ])
      .execute();

    const matches = await collabQueryService.getMusicians(userLocation, { genre: "jazz" });

    expect(matches).toHaveLength(1);
    expect(matches[0].name === "marcy").toBeTruthy();
  });

  it("can search by given filters", async () => {
    // Get from device GPS
    const userLocation = new Location(15.143262, 120.583843);

    await connection
      .createQueryBuilder()
      .insert()
      .into(musicianEntity)
      .values([
        {
          id: new MusicianId(faker.random.uuid()),
          name: "finn",
          genre: "metal",
          role: "guitarist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 5)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "jake",
          genre: "metal",
          role: "guitarist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 10)
        },
        {
          id: new MusicianId(faker.random.uuid()),
          name: "marcy",
          genre: "jazz",
          role: "bassist",
          location: generateRelativeLocation(userLocation.latitude, userLocation.longitude, 15)
        }
      ])
      .execute();

    const matches = await collabQueryService.getMusicians(userLocation, {
      maxDistance: 10,
      genre: "metal",
      role: "guitarist"
    });

    expect(matches).toHaveLength(2);
    expect(matches.some(match => match.name === "marcy")).toBeFalsy();
  });
});
