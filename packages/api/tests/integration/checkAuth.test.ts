import dotenv from "dotenv";
import { default as firebase } from "firebase";
import { default as admin } from "firebase-admin";
import "firebase/auth";
import { default as request } from "supertest";
import { default as createApplication } from "../../src/app";
import { default as createContainer } from "../../src/container";
import checkAuth from "../../src/web/authenticated";
import { default as createRoutes } from "../../src/web/routes";

dotenv.config();

describe("firebase checkAuth middleware", () => {
  const container = createContainer();
  const router = createRoutes(container);
  let server;

  router.get("/test", checkAuth, context => {
    context.response.body = "hello there";
  });

  beforeAll(async () => {
    const app = await createApplication(container, router);
    server = app.listen();
  });

  afterAll(async () => {
    await server.close();
  });

  it("should return 401 if no token", async () => {
    await request(server)
      .get("/test")
      .expect(401);
  });

  it("should return 401 if has invalid token", async () => {
    await request(server)
      .get("/test")
      .set("Authorization", "Bearer abc123.ThisIsInvalid.token")
      .expect(401);
  });

  it("should return 200 if has valid token", async () => {
    firebase.initializeApp(require(process.env.FIREBASE_CONFIG));

    const idToken = await admin
      .auth()
      .createCustomToken("9bc9e6d9-a22b-4d75-b743-be26482933aa")
      .then(customToken => {
        return firebase.auth().signInWithCustomToken(customToken);
      })
      .then(({ user }) => {
        return user.getIdToken();
      });

    await request(server)
      .get("/test")
      .set("Authorization", `Bearer ${idToken}`)
      .expect(200);
  });
});
