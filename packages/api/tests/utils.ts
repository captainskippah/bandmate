import { Location } from "@bandmate/types/musician";
import { toRadians } from "../src/utils/location";

export function generateRelativeLocation(
  latitude: number,
  longitude: number,
  distance: number
): Location {
  // Wikipedia suggested approx 111.32km per degree
  const radiusInDegrees = (distance * 1000) / 111320;

  // random angle
  const t = 2 * Math.PI * Math.random();

  const y = radiusInDegrees * Math.sin(t);
  const x = (radiusInDegrees * Math.cos(t)) / Math.cos(toRadians(latitude));

  const newLatitude = latitude + y;
  const newLongitude = longitude + x;

  return {
    latitude: newLatitude,
    longitude: newLongitude
  };
}
