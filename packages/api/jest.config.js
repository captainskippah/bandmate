module.exports = {
  roots: [
    "<rootDir>/src",
    "<rootDir>/tests"
  ],
  preset: "ts-jest",
  globals: {
    "ts-jest": {
      diagnostics: {
        warnOnly: true
      }
    }
  },
  moduleFileExtensions: [
    "ts",
    "js"
  ],
  testEnvironment: "node"
};
