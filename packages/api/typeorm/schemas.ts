import { Genre, Location, Musician, Role } from "@bandmate/types/musician";
import { Point } from "geojson";
import { EntitySchema } from "typeorm";
import { toGeoJSON } from "../src/utils/location";

export interface GenreSchema {
  id: Genre;
}

export const genreEntity = new EntitySchema<GenreSchema>({
  name: "Genre",
  columns: {
    id: {
      type: "text",
      primary: true
    }
  }
});

export interface RoleSchema {
  id: Role;
}

export const roleEntity = new EntitySchema<RoleSchema>({
  name: "Role",
  columns: {
    id: {
      type: "text",
      primary: true
    }
  }
});

export interface MusicianSchema {
  id: string;
  bio: string;
  roles: RoleSchema[];
  genres: GenreSchema[];
  location?: Location;
}

export const musicianEntity = new EntitySchema<MusicianSchema>({
  name: "Musician",
  columns: {
    id: {
      type: "text",
      primary: true
    },
    bio: {
      type: "text"
    },
    location: {
      type: "geography",
      nullable: true,
      spatialFeatureType: "Point",
      transformer: {
        from(value: Point | null): Location | undefined {
          return value === null
            ? undefined
            : {
                latitude: value.coordinates[1],
                longitude: value.coordinates[0]
              };
        },
        to(value: Location | undefined): Point | null {
          return value === undefined ? null : toGeoJSON(value);
        }
      }
    }
  },
  relations: {
    roles: {
      target: "Role",
      type: "many-to-many",
      joinTable: {
        name: "musician_role",
        joinColumn: {
          name: "musician",
          referencedColumnName: "id"
        },
        inverseJoinColumn: {
          name: "role",
          referencedColumnName: "id"
        }
      }
    },
    genres: {
      target: "Genre",
      type: "many-to-many",
      joinTable: {
        name: "musician_genre",
        joinColumn: {
          name: "musician",
          referencedColumnName: "id"
        },
        inverseJoinColumn: {
          name: "genre",
          referencedColumnName: "id"
        }
      }
    }
  }
});
