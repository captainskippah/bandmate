import { RoleSchema } from "../schemas";
import { Seeds } from "./types";

const roles: Seeds<RoleSchema> = {
  entity: "role",
  seeds: [
    { id: "bassist" },
    { id: "drummer" },
    { id: "guitarist" },
    { id: "keyboardist" },
    { id: "vocalist" }
  ]
};

export default roles;
