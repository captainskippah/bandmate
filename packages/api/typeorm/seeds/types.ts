export interface Seeds<T> {
  entity: string;
  seeds: T[];
}
