import { GenreSchema } from "../schemas";
import { Seeds } from "./types";

const genres: Seeds<GenreSchema> = {
  entity: "genre",
  seeds: [
    { id: "blues" },
    { id: "jazz" },
    { id: "metal" },
    { id: "pop" },
    { id: "reggae" },
    { id: "rock" }
  ]
};

export default genres;
