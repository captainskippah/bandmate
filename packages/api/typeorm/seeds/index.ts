import genres from "./genres";
import roles from "./roles";

const seeds = [genres, roles];

export default seeds;
