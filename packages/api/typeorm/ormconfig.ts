import { ConnectionOptions } from "typeorm";
import { database } from "../src/config";
import { UserDeviceToken1571791444422 } from "./migrations/userDeviceToken";
import { genreEntity, musicianEntity, roleEntity } from "./schemas";

export default {
  type: "postgres",
  host: database.host,
  port: database.port,
  username: database.username,
  password: database.password,
  database: database.database,
  ssl: process.env.NODE_ENV === "production",
  synchronize: process.env.NODE_ENV !== "production",
  logging: true,
  entities: [musicianEntity, roleEntity, genreEntity],
  migrations: [UserDeviceToken1571791444422]
} as ConnectionOptions;
