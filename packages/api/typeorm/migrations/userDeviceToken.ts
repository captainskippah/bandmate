import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class UserDeviceToken1571791444422 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const table = new Table({
      name: "user_device",
      columns: [
        {
          name: "musician",
          type: "text"
        },
        {
          name: "token",
          type: "text",
          isUnique: true
        }
      ],
      foreignKeys: [
        {
          columnNames: ["musician"],
          referencedTableName: "musician",
          referencedColumnNames: ["id"],
          onDelete: "cascade"
        }
      ]
    });

    await queryRunner.createTable(table, true);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable("user_device");
  }
}
