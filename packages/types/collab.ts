import { Genre, Role } from "./musician";

export interface CollabFilter {
  maxDistance?: number;
  role?: Role;
  genre?: Genre;
}
