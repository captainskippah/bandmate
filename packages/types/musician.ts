export type Role = "vocalist" | "guitarist" | "bassist" | "keyboardist" | "drummer";

export type Genre = "rock" | "metal" | "pop" | "blues" | "jazz" | "reggae";

export interface Location {
  latitude: number;
  longitude: number;
}

export interface Musician {
  id: string;
  name: string;
  image: string;
  bio: string;
  roles: Role[];
  genres: Genre[];
  location?: Location;
}

export interface MusicianView {
  id: string;
  name: string;
  image: string;
  bio: string;
  roles: Role[];
  genres: Genre[];
  location: string;
}
