# BandMate - website

Website for BandMate

## Build Setup

``` bash
# install dependencies
$ yarn install

# build  files
$ yarn run generate

# watch mode (development)
$ yarn run watch
```

## Deploying to Firebase

Start by logging in to firebase by typing:

```bash
$ yarn firebase login
```

You only have to do this once.

You can deploy the `dist/` directory by typing:

``` bash
$ yarn firebase deploy
```
