const { src, dest, watch, series } = require('gulp');
const postcss = require('gulp-postcss');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const del = require('del');
const autoprefixer = require('autoprefixer');
const tailwindcss = require('tailwindcss');

function css() {
  const postcssPlugins = [
    tailwindcss,
    autoprefixer
  ];

  if (process.env.NODE_ENV === 'production') {
    const purgeCss = require("@fullhuman/postcss-purgecss")({
      content: [
        './src/*.html'
      ],
      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
    });

    postcssPlugins.push(purgeCss)
  }

  const pipeline = src('src/css/tailwind.css')
    .pipe(postcss(postcssPlugins));

  if (process.env.NODE_ENV === 'production') {
    pipeline.pipe(cleanCSS())
  }

  return pipeline
    .pipe(rename(function (path) {
      path.basename = 'style'
    }))
    .pipe(dest('dist/css/'));
}

function clean() {
  return del('dist/');
}

function copy() {
  return src(['src/**', '!src/css/**'])
    .pipe(dest('dist/'));
}

const generate = series(clean, copy, css);

function watchCss() {
  watch('src/css/*.css', css)
}

exports.watch = series(generate, watchCss);
exports.default = generate;
