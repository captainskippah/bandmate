package com.bandmate.utils;

import android.location.Location;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.Promise;

class LocationUtil extends ReactContextBaseJavaModule {

   public LocationUtil(ReactApplicationContext reactContext) {
     super(reactContext);
   }

  @Override
  public String getName() {
      return "RNLocationUtil";
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  public float distanceBetween(ReadableMap location, ReadableMap location2) {
    Location aLocation = new Location("");

    aLocation.setLatitude(location.getDouble("latitude"));
    aLocation.setLongitude(location.getDouble("longitude"));

    Location aLocation2 = new Location("");

    aLocation2.setLatitude(location2.getDouble("latitude"));
    aLocation2.setLongitude(location2.getDouble("longitude"));

    return aLocation.distanceTo(aLocation2);
  }
}
