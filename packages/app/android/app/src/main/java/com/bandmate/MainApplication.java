package com.bandmate;

import com.BV.LinearGradient.LinearGradientPackage;
import com.oblador.vectoricons.VectorIconsPackage;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.soloader.SoLoader;

import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;

import io.invertase.firebase.app.ReactNativeFirebaseAppPackage;
import io.invertase.firebase.auth.ReactNativeFirebaseAuthPackage;
import io.invertase.firebase.crashlytics.ReactNativeFirebaseCrashlyticsPackage;
import io.invertase.firebase.storage.ReactNativeFirebaseStoragePackage;

import com.agontuk.RNFusedLocation.RNFusedLocationPackage;

import com.bandmate.utils.LocationUtilPackage;

import com.imagepicker.ImagePickerPackage;

import com.dylanvann.fastimage.FastImageViewPackage;

import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;

import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {

    @Override
    protected ReactGateway createReactGateway() {
        ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
            @Override
            protected String getJSMainModuleName() {
                return "index";
            }
        };
        return new ReactGateway(this, isDebug(), host);
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
            new LinearGradientPackage(),
            new VectorIconsPackage(),
            new ReactNativeFirebaseAppPackage(),
            new ReactNativeFirebaseAuthPackage(),
            new ReactNativeFirebaseCrashlyticsPackage(),
            new ReactNativeFirebaseStoragePackage(),
            new FBSDKPackage(),
            new RNFusedLocationPackage(),
            new GeocoderPackage(),
            new LocationUtilPackage(),
            new ImagePickerPackage(),
            new FastImageViewPackage(),
            new ReactNativePushNotificationPackage(),
            new ReactNativeConfigPackage()
        );
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }
}
