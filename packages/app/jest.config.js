module.exports = {
  preset: "react-native",
  moduleFileExtensions: ["ts", "tsx"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  transformIgnorePatterns: [
    "node_modules/(?!((jest-)?react-native|react-navigation|@react-navigation/.*))"
  ],
  testRegex: "(/tests/.*|\\.test)\\.(ts|tsx)$",
  testPathIgnorePatterns: ["<rootDir>/node_modules/"],
  globals: {
    "ts-jest": {
      babelConfig: true
    }
  },
  cacheDirectory: ".jest/cache"
};
