# BandMate - app

A Tinder-like app for musicians who are looking for others they can collaborate with.

## Environment Variables

Start by creating `.env`, `.env.staging`, and `.env.production`.

All 3 env files has the following Environment Variables:

- `API_URL` - URL to `@bandmate/api` server.
  - Dev - this should be your machine's LAN address.
  - Staging - this should be our staging heroku URL.
  - Production - this should be our production heroku URL.
- `CHATKIT_INSTANCE` - Can be found on Chatkit dashboard under the name of `CHATKIT_LOCATOR_ID`.
- `CHATKIT_TOKEN_URL` - Chatkit test token URL. Can be found on Chatkit dashboard.

## Build Setup

Start by installing the dependencies:

``` bash
# install dependencies
$ yarn install 
```

You can then install the app to your device with:

```bash
# install app to device
$ yarn install:<build type>
```

Build type can be any of these 3: `debug`, `staging`, or `release`.

If you are installing `debug` build, you have to run the Metro server by typing:

```bash
# running Metro server
$ yarn dev
```

## Publishing to Firebase App Distribution

This is for sending `staging` build to our testers with Firebase App Distribution. Play Store is slow because it has to review every uploads of any build types.

You can build the `staging` build by typing:

```bash
# generating Staging build
$ yarn build:staging
```

You can find the generated `.apk` file under `android/app/build/outputs/apk/staging`.

**Note**: If you recently installed the `staging` build on your device, you don't have to do the above steps because `yarn install:staging` depends on `yarn build:staging`.

## Publishing to Play Store

**Important**: Don't forget to increment `versionCode` and update `versionName` inside `android/app/build.gradle`.

Play Store recommends uploading [App Bundle](https://developer.android.com/platform/technology/app-bundle) and you can do so by typing:

```bash
# generating App Bundle
$ yarn bundle:release
```

You can find the generated `.aab` file under `android/app/build/outputs/bundle/release`.
