module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    [
      "module-resolver",
      {
        root: ["./"],
        alias: {
          "@actions": "./src/actions",
          "@assets": "./src/assets",
          "@components": "./src/components",
          "@core": "./src/core",
          "@screens": "./src/screens",
          "@stores": "./src/stores",
          "@utils": "./src/utils"
        }
      }
    ]
  ],
  env: {
    production: {
      plugins: ["transform-remove-console"]
    }
  }
};
