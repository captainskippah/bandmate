import AuthService from "@core/auth/AuthService";
import CollabService from "@core/collab/CollabService";
import MessageService from "@core/messages/MessageService";
import crashlytics from "@react-native-firebase/crashlytics";
import { default as registerAuthScreen } from "@screens/auth/register";
import { default as registerCollabScreen } from "@screens/collab/register";
import { default as registerMessagesScreen } from "@screens/messages/register";
import { default as registerProfileScreen } from "@screens/profile/register";
import MessagesStore from "@stores/messages/store";
import UserStore from "@stores/user/store";
import { IReactionDisposer, reaction, when } from "mobx";
import { LayoutRoot, Navigation } from "react-native-navigation";
import Icon from "react-native-vector-icons/MaterialIcons";

type LayoutNames = ["auth", "home"];

type Layouts = {
  [key in LayoutNames[number]]: LayoutRoot;
};

let initialized = false;
let layouts: Layouts | undefined;

interface UIStores {
  messagesStore: MessagesStore;
  userStore: UserStore;
}

interface Services {
  authService: AuthService;
  collabService: CollabService;
  messagesService: MessageService;
}

export function initializeLayouts(stores: UIStores, services: Services) {
  console.log("Initializing layouts");

  const { userStore, messagesStore } = stores;
  const { authService, collabService } = services;

  registerAuthScreen(authService, userStore);

  return Promise.all([
    Icon.getImageSource("people"),
    Icon.getImageSource("message"),
    Icon.getImageSource("person")
  ])
    .then(icons => {
      let screenRegistered = false;
      let unreadReactionDisposer: IReactionDisposer | undefined;

      layouts = {
        auth: {
          root: {
            component: {
              name: "auth.LoginScreen",
              passProps: {
                async onLogin() {
                  if (!screenRegistered) {
                    console.log("Setting up Authenticated Screens");
                    registerCollabScreen(userStore, collabService);
                    registerMessagesScreen(messagesStore);
                    registerProfileScreen(userStore);
                    screenRegistered = true;
                  }

                  when(
                    () => userStore.isRegistered(),
                    () => {
                      console.log("When: User is registered.");

                      messagesStore.initialize().then(() => {
                        unreadReactionDisposer = reaction<number>(
                          () => messagesStore.unreadCount,
                          count => {
                            Navigation.mergeOptions("messages.MessagesScreen", {
                              bottomTab: {
                                badge: count === 0 ? "" : count.toString()
                              }
                            });
                          },
                          {
                            fireImmediately: true
                          }
                        );
                      });
                    }
                  );

                  await userStore.initialize();

                  if (!userStore.isRegistered()) {
                    await Navigation.setRoot({
                      root: {
                        component: {
                          name: "profile.EditProfileScreen",
                          passProps: {
                            firstTime: true
                          }
                        }
                      }
                    });
                  } else {
                    await goToHomeScreen();
                  }
                }
              }
            }
          }
        },

        home: {
          root: {
            bottomTabs: {
              id: "BottomTabs",
              children: [
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: "collab.CollabScreen"
                        }
                      }
                    ],
                    options: {
                      bottomTab: {
                        text: "Collab",
                        icon: icons[0]
                      }
                    }
                  }
                },
                {
                  stack: {
                    children: [
                      {
                        component: {
                          id: "messages.MessagesScreen",
                          name: "messages.MessagesScreen"
                        }
                      }
                    ],
                    options: {
                      bottomTab: {
                        text: "Messages",
                        icon: icons[1]
                      }
                    }
                  }
                },
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: "profile.MeScreen",
                          passProps: {
                            async logout() {
                              await userStore.logout();

                              console.log("User logged out. Cleaning up now.");

                              unreadReactionDisposer!();
                              userStore.dispose();
                              messagesStore.dispose();
                              authService.dispose();

                              goToAuthScreen();
                            }
                          }
                        }
                      }
                    ],
                    options: {
                      bottomTab: {
                        text: "Me",
                        icon: icons[2]
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      };

      initialized = true;

      console.log("Layouts initialized");
    })
    .catch(error => {
      if (__DEV__) {
        console.error("Layouts failed to initialize.", error);
      } else {
        crashlytics().log("Layouts failed to initialize.");
        crashlytics().recordError(error);
      }
      return Promise.reject(error);
    });
}

export function goToAuthScreen() {
  if (!initialized) {
    throw new Error(
      "Layouts not initialized. Execute `initializeLayouts` first."
    );
  }

  return Navigation.setRoot(layouts!.auth);
}

export function goToHomeScreen() {
  if (!initialized) {
    throw new Error(
      "Layouts not initialized. Execute `initializeLayouts` first."
    );
  }

  return Navigation.setRoot(layouts!.home);
}
