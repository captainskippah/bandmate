import openConversationWith from "@actions/openConversationWith";
import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import { CollabFilter } from "@bandmate/types/collab";
import { Musician, MusicianView } from "@bandmate/types/musician";
import Loading from "@components/Loading";
import Text from "@components/Text";
import CollabService from "@core/collab/CollabService";
import { UserStore } from "@stores/index";
import Geocoder from "@utils/Geocoder";
import Location from "@utils/Location";
import isEqual from "lodash.isequal";
import React, { RefObject } from "react";
import {
  Alert,
  Animated,
  Dimensions,
  RefreshControl,
  StyleSheet,
  View
} from "react-native";
import { Button, Icon, ThemeProvider } from "react-native-elements";
import { Navigation } from "react-native-navigation";
import { ProfileScreenProps } from "../profile/ProfileScreen";
import { CollabFilterProps } from "./CollabFilterScreen";
import MusicianCard from "./MusicianCard";

const SCREEN_WIDTH = Dimensions.get("window").width;

export interface CollabScreenProps {
  userStore: UserStore;
  collabService: CollabService;
}

interface CollabScreenState {
  fetching: boolean;
  filter: CollabFilter;
  profiles: MusicianView[];
  scrollOffset: Animated.Value;
}

export default class CollabScreen extends React.Component<
  CollabScreenProps,
  CollabScreenState
> {
  public static options() {
    return {
      topBar: {
        visible: false
      },
      bottomTabs: {
        visible: true
      }
    };
  }

  private readonly collabService: CollabService;

  private mounted: boolean;

  constructor(props) {
    super(props);

    this.state = {
      fetching: false,
      filter: {
        maxDistance: 30,
        role: undefined,
        genre: undefined
      },
      profiles: [],
      scrollOffset: new Animated.Value(0)
    };

    this.collabService = props.collabService;
    this.mounted = false;
  }

  public componentDidMount() {
    this.mounted = true;

    if (this.props.userStore.locationError === undefined) {
      this.fetchProfiles();
    }
  }

  public componentDidUpdate(prevProps, prevState: CollabScreenState) {
    // We compare by reference because we trust our shouldComponentUpdate
    // to not call componentDidUpdate when not necessary.
    // Also it is easier to read.

    // New `ref` will be set whenever `profiles` changes
    // However they are not reactive. Manually update.
    if (prevState.profiles !== this.state.profiles) {
      this.forceUpdate();
    }

    if (prevState.filter !== this.state.filter) {
      this.fetchProfiles();
    }
  }

  public componentWillUnmount() {
    this.mounted = false;
  }

  public render() {
    return (
      <ThemeProvider theme={theme}>
        <View style={{ flex: 1 }}>
          {this.state.fetching ? (
            <Loading />
          ) : this.props.userStore.locationError !== undefined ? (
            <View
              style={{
                height: "100%",
                padding: spacing.spacing4,
                justifyContent: "center"
              }}
            >
              <Text text="Uh oh..." type="h4" />
              <Text
                text="BandMate can't access your location."
                type="body"
                style={{ marginTop: spacing.spacing3 }}
              />
              <Text
                text="Try turning on your location or granting the app to use your location."
                type="body"
                style={{ marginTop: spacing.spacing2 }}
              />
              <Button
                title="Grant Location"
                onPress={this.initializeLocationAndFetchProfiles}
                buttonStyle={{ backgroundColor: colors.blue }}
                containerStyle={{ marginTop: spacing.spacing6 }}
              />
            </View>
          ) : (
            <Animated.FlatList
              data={this.state.profiles}
              keyExtractor={item => item.id}
              renderItem={({ item, index }) => (
                <MusicianCard
                  profile={item}
                  onPress={this.viewProfile(index)}
                  onMessagePress={this.messageProfile(index)}
                  style={[{ width: SCREEN_WIDTH }, this.getCardStyle(index)]}
                />
              )}
              ListEmptyComponent={
                <Text
                  text={`No profiles available\nConsider updating your filters`}
                  type="body"
                  style={{
                    color: colors.gray,
                    textAlign: "center"
                  }}
                />
              }
              refreshControl={
                <RefreshControl
                  refreshing={this.state.fetching}
                  onRefresh={this.onRefresh}
                  progressViewOffset={56}
                  enabled={this.state.profiles.length === 0}
                />
              }
              horizontal={true}
              pagingEnabled={true}
              showsHorizontalScrollIndicator={false}
              scrollEventThrottle={16}
              onScroll={Animated.event(
                [
                  {
                    nativeEvent: {
                      contentOffset: { x: this.state.scrollOffset }
                    }
                  }
                ],
                { useNativeDriver: true }
              )}
              initialNumToRender={1}
              getItemLayout={(data, index) => ({
                index,
                length: SCREEN_WIDTH,
                offset: SCREEN_WIDTH * index
              })}
              contentContainerStyle={
                this.state.profiles.length === 0
                  ? {
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center"
                    }
                  : null
              }
            />
          )}
          <View style={styles.header}>
            <Text
              type="h6"
              text="Collaborate"
              style={{ marginLeft: spacing.spacing4 }}
            />
            <Icon
              name="tune"
              color={colors.white}
              iconStyle={{ padding: spacing.spacing2 }}
              onPress={this.openFilter}
              containerStyle={{ marginRight: spacing.spacing2 }}
            />
          </View>
        </View>
      </ThemeProvider>
    );
  }

  private openFilter = () => {
    Navigation.showModal<CollabFilterProps>({
      component: {
        id: "collab.CollabFilterScreen",
        name: "collab.CollabFilterScreen",
        passProps: {
          filter: this.state.filter,
          onApplyFilter: (filter: CollabFilter) => {
            this.applyFilter(filter);
            Navigation.dismissModal("collab.CollabFilterScreen");
          }
        }
      }
    });
  };

  private applyFilter = (filter: CollabFilter) => {
    this.setState({ filter });
  };

  private onRefresh = () => {
    if (this.props.userStore.locationError !== undefined) {
      Alert.alert(
        "Location Permission",
        "Band Mate needs your location so that it can find you musicians in your area.",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          {
            text: "Allow",
            onPress: this.initializeLocationAndFetchProfiles
          }
        ]
      );
    } else {
      this.fetchProfiles();
    }
  };

  private initializeLocationAndFetchProfiles = () => {
    this.props.userStore.initializeLocation().then(() => {
      this.fetchProfiles();
    });
  };

  private fetchProfiles = () => {
    if (
      this.state.fetching ||
      this.props.userStore.locationError !== undefined
    ) {
      return;
    }

    this.setState({ fetching: true });

    this.collabService.get(this.state.filter).then(async profiles => {
      if (this.mounted) {
        this.setState({
          profiles: await Promise.all(profiles.map(this.formatProfile)),
          fetching: false
        });
      }
    });
  };

  private formatProfile = async (musician: Musician): Promise<MusicianView> => {
    const place = await Geocoder.geocodePosition(musician.location!);

    const distance = Location.distanceBetween(
      this.props.userStore.getLocation()!,
      musician.location!
    );

    return {
      ...musician,
      location: `${Math.round(distance / 1000)} kilometers - ${place}`
    };
  };

  private viewProfile = index => () => {
    Navigation.showModal<ProfileScreenProps>({
      stack: {
        children: [
          {
            component: {
              name: "profile.ProfileScreen",
              passProps: {
                ...this.state.profiles[index]
              }
            }
          }
        ]
      }
    });
  };

  private messageProfile = index => () => {
    const { id, name, image } = this.state.profiles[index];
    // @ts-ignore
    openConversationWith(this.props.componentId, { id, name, image });
  };

  private getCardStyle = index => {
    const scale = this.state.scrollOffset.interpolate({
      inputRange: [
        (index - 1) * SCREEN_WIDTH,
        index * SCREEN_WIDTH,
        (index + 1) * SCREEN_WIDTH
      ],
      outputRange: [0.75, 1, 0.75],
      extrapolate: "clamp"
    });

    return {
      transform: [{ scale }]
    };
  };
}

const styles = StyleSheet.create({
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    height: 56,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  }
});
