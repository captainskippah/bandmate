import CollabService from "@core/collab/CollabService";
import registerComponentWithProvider from "@screens/utils";
import { UserStore } from "@stores/index";
import { Navigation } from "react-native-navigation";
import CollabFilterScreen from "./CollabFilterScreen";
import CollabScreen from "./CollabScreen";

export default function(userStore: UserStore, collabService: CollabService) {
  registerComponentWithProvider("collab.CollabScreen", CollabScreen, {
    userStore,
    collabService
  });

  Navigation.registerComponent(
    "collab.CollabFilterScreen",
    () => CollabFilterScreen
  );
}
