import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import { CollabFilter } from "@bandmate/types/collab";
import { Genre, Role } from "@bandmate/types/musician";
import Chip from "@components/Chip";
import Text from "@components/Text";
import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, Slider, ThemeProvider } from "react-native-elements";

export interface CollabFilterProps {
  filter: CollabFilter;
  onApplyFilter: (CollabFilter) => void;
}

export default class CollabFilterScreen extends React.Component<
  CollabFilterProps,
  CollabFilter
> {
  constructor(props: CollabFilterProps) {
    super(props);

    this.state = {
      ...props.filter
    };
  }

  public render() {
    return (
      <ThemeProvider theme={theme}>
        <View style={{ flex: 1 }}>
          <View style={styles.header}>
            <Text
              type="h6"
              text="Filters"
              style={{ marginLeft: spacing.spacing4 }}
            />
            <Button
              title="Apply"
              titleStyle={{ color: colors.white }}
              type="clear"
              onPress={this.onApplyPress}
              containerStyle={{ marginRight: spacing.spacing2 }}
            />
          </View>

          <View style={styles.filterGroup}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                marginBottom: spacing.spacing4
              }}
            >
              <Text
                text="Maximum Distance"
                type="subtitle2"
                style={[styles.filterTitle, { marginBottom: 0 }]}
              />
              <Text
                type="body2"
                text={`${this.state.maxDistance} km`}
                style={{
                  width: "25%",
                  color: colors.gray,
                  textAlign: "right"
                }}
              />
            </View>
            <Slider
              minimumValue={10}
              maximumValue={100}
              step={10}
              thumbTintColor={colors.white}
              value={this.state.maxDistance}
              onValueChange={value => this.setState({ maxDistance: value })}
            />
          </View>

          <View style={styles.filterGroup}>
            <Text text="Role" type="subtitle2" style={styles.filterTitle} />
            <View style={styles.tags}>
              <Chip
                text="Any"
                onPress={() => this.setState({ role: undefined })}
                highlighted={this.state.role === undefined}
                style={styles.tag}
              />
              <Chip
                text="Vocalist"
                onPress={() => this.selectRole("vocalist")}
                highlighted={this.isRoleSelected("vocalist")}
                style={styles.tag}
              />
              <Chip
                text="Guitarist"
                onPress={() => this.selectRole("guitarist")}
                highlighted={this.isRoleSelected("guitarist")}
                style={styles.tag}
              />
              <Chip
                text="Bassist"
                onPress={() => this.selectRole("bassist")}
                highlighted={this.isRoleSelected("bassist")}
                style={styles.tag}
              />
              <Chip
                text="Drummer"
                onPress={() => this.selectRole("drummer")}
                highlighted={this.isRoleSelected("drummer")}
                style={styles.tag}
              />
            </View>
          </View>

          <View style={styles.filterGroup}>
            <Text text="Genres" type="subtitle2" style={styles.filterTitle} />
            <View style={styles.tags}>
              <Chip
                text="Any"
                onPress={() => this.setState({ genre: undefined })}
                highlighted={this.state.genre === undefined}
                style={styles.tag}
              />
              <Chip
                text="Rock"
                onPress={() => this.selectGenre("rock")}
                highlighted={this.isGenreSelected("rock")}
                style={styles.tag}
              />
              <Chip
                text="Metal"
                onPress={() => this.selectGenre("metal")}
                highlighted={this.isGenreSelected("metal")}
                style={styles.tag}
              />
              <Chip
                text="Pop"
                onPress={() => this.selectGenre("pop")}
                highlighted={this.isGenreSelected("pop")}
                style={styles.tag}
              />
              <Chip
                text="Blues"
                onPress={() => this.selectGenre("blues")}
                highlighted={this.isGenreSelected("blues")}
                style={styles.tag}
              />
              <Chip
                text="Jazz"
                onPress={() => this.selectGenre("jazz")}
                highlighted={this.isGenreSelected("jazz")}
                style={styles.tag}
              />
              <Chip
                text="Reggae / Ska"
                onPress={() => this.selectGenre("reggae")}
                highlighted={this.isGenreSelected("reggae")}
                style={styles.tag}
              />
            </View>
          </View>
        </View>
      </ThemeProvider>
    );
  }

  private onApplyPress = () => {
    this.props.onApplyFilter({ ...this.state });
  };

  private selectRole = (role: Role) => {
    this.setState({ role });
  };

  private isRoleSelected = (role: Role) => {
    return this.state.role === role;
  };

  private selectGenre = (genre: Genre) => {
    this.setState({ genre });
  };

  private isGenreSelected = (genre: Genre) => {
    return this.state.genre === genre;
  };
}

const styles = StyleSheet.create({
  header: {
    height: 56,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  filterGroup: {
    paddingTop: spacing.spacing3,
    paddingBottom: spacing.spacing4,
    paddingHorizontal: spacing.spacing4
  },

  filterTitle: {
    color: colors.gray,
    marginBottom: spacing.spacing4
  },

  tags: {
    marginHorizontal: -spacing.spacing2,
    marginBottom: -spacing.spacing3,
    flexDirection: "row",
    flexWrap: "wrap"
  },

  tag: {
    marginHorizontal: spacing.spacing2,
    marginBottom: spacing.spacing3
  }
});
