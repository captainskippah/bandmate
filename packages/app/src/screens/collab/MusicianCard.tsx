import colors from "@assets/colors";
import spacing from "@assets/spacing";
import { MusicianView } from "@bandmate/types/musician";
import Scrim from "@components/Scrim";
import Text from "@components/Text";
import React from "react";
import {
  Animated,
  StyleSheet,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import FastImage from "react-native-fast-image";

export interface MusicianCardProp {
  profile: MusicianView;
  onSwipeLeft?: () => void;
  onSwipeRight?: () => void;
  onPress?: () => void;
  onMessagePress?: (id: string, name: string, image: string) => void;
  style?: any;
}

export default class MusicianCard extends React.PureComponent<
  MusicianCardProp,
  {}
> {
  constructor(props: MusicianCardProp) {
    super(props);
  }

  public render() {
    const { image, name, bio, location } = this.props.profile;
    return (
      <Animated.View style={[this.props.style]}>
        <TouchableWithoutFeedback onPress={this.onPress}>
          <View>
            <View>
              <FastImage source={{ uri: image }} style={styles.image} />
            </View>

            <View style={styles.cardTextContainer}>
              <Scrim />
              <View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    type="h4"
                    text={name}
                    style={{
                      flex: 1,
                      fontWeight: "700",
                      flexWrap: "wrap"
                    }}
                  />
                  <Icon
                    name="message"
                    color={colors.white}
                    iconStyle={{ padding: spacing.spacing3 }}
                    containerStyle={{
                      alignSelf: "flex-end",
                      marginBottom: -spacing.spacing2
                    }}
                    onPress={this.onMessagePress}
                  />
                </View>
                <Text type="body" text={location} />
              </View>
              <View style={{ marginTop: spacing.spacing4 }}>
                <Text type="body" text={bio} />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }

  private onPress = () => {
    if (this.props.onPress !== undefined) {
      this.props.onPress();
    }
  };

  private onMessagePress = () => {
    if (this.props.onMessagePress !== undefined) {
      const { id, name, image } = this.props.profile;
      this.props.onMessagePress(id, name, image);
    }
  };
}

const styles = StyleSheet.create({
  cardTextContainer: {
    paddingTop: 100,
    paddingHorizontal: spacing.spacing8,
    paddingBottom: spacing.spacing8,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0
  },

  image: {
    width: "100%",
    height: "100%"
  }
});
