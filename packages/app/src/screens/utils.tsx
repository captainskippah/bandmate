import React from "react";
import { Navigation } from "react-native-navigation";

export default function registerComponentWithProvider(
  componentId: string,
  ScreenComponent: React.ComponentType<any>,
  provides: object
) {
  Navigation.registerComponent(
    componentId,
    () => props => <ScreenComponent {...props} {...provides} />,
    () => ScreenComponent
  );
}
