import AuthService from "@core/auth/AuthService";
import LoginScreen from "@screens/auth/LoginScreen";
import registerComponentWithProvider from "@screens/utils";
import { UserStore } from "@stores/index";

export default function(authService: AuthService, userStore: UserStore) {
  registerComponentWithProvider("auth.LoginScreen", LoginScreen, {
    authService,
    userStore
  });
}
