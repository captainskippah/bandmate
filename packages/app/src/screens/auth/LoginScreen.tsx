import colors from "@assets/colors";
import background from "@assets/login.png";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import AuthService from "@core/auth/AuthService";
import crashlytics from "@react-native-firebase/crashlytics";
import { UserStore } from "@stores/index";
import React, { Component } from "react";
import {
  ActivityIndicator,
  Alert,
  BackHandler,
  Dimensions,
  ImageBackground,
  StyleSheet,
  View
} from "react-native";
import { Button, Text, ThemeProvider } from "react-native-elements";

export interface LoginScreenProps {
  authService: AuthService;
  userStore: UserStore;
  onLogin: () => void | Promise<void>;
}

interface LoginScreenState {
  loading: boolean;
}

export default class LoginScreen extends Component<
  LoginScreenProps,
  LoginScreenState
> {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };
  }

  public componentDidMount() {
    this.props.authService.initialize().then(() => {
      if (this.props.authService.isAuthenticated()) {
        this.props.onLogin();
      } else {
        this.setState({
          loading: false
        });
      }
    });
  }

  public render() {
    return (
      <ThemeProvider theme={theme}>
        <ImageBackground source={background} style={styles.imageBackground}>
          <View style={styles.overlay}>
            <View style={styles.headerContainer}>
              <Text style={{ fontSize: 40 }}>BandMate</Text>
              <Text style={{ marginTop: spacing.spacing2 }}>
                Collaborate with musicians in your area
              </Text>
            </View>
            {this.state.loading ? (
              <View style={styles.activityContainer}>
                <ActivityIndicator size="large" color={colors.white} />
                <Text
                  style={{ color: colors.white, marginTop: spacing.spacing6 }}
                >
                  Loading...
                </Text>
              </View>
            ) : (
              <Button
                title="Login with Facebook"
                type="solid"
                buttonStyle={{ backgroundColor: colors.blue }}
                titleStyle={{ color: colors.white }}
                containerStyle={{ marginTop: spacing.spacing4 }}
                onPress={this.facebookLogin}
              />
            )}
          </View>
        </ImageBackground>
      </ThemeProvider>
    );
  }

  private facebookLogin = async () => {
    try {
      this.setState({ loading: true });

      if (await this.props.authService.facebookLogin()) {
        await this.props.onLogin();
      }
    } catch (e) {
      if (__DEV__) {
        console.error("facebookLogin error", e);
      } else {
        crashlytics().log("facebookLogin error");
        crashlytics().recordError(e);
      }

      this.setState({
        loading: false
      });

      Alert.alert(
        "Login Failed",
        "Oopsie doopsie, something went wrong. I am working on getting this fixed as soon as I can. Please come back again later.",
        [
          {
            text: "I Understand",
            onPress() {
              BackHandler.exitApp();
            }
          }
        ]
      );
    }
  };
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  imageBackground: {
    height,
    width
  },
  overlay: {
    flex: 1,
    padding: spacing.spacing8,
    backgroundColor: "rgba(13, 13, 13, 0.6)"
  },
  headerContainer: {
    paddingTop: 56,
    paddingBottom: 56
  },
  activityContainer: {
    alignItems: "center",
    justifyContent: "center"
  }
});
