import openConversationWith from "@actions/openConversationWith";
import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import { MusicianView } from "@bandmate/types/musician";
import Chip from "@components/Chip";
import Text from "@components/Text";
import React from "react";
import {
  Animated,
  Dimensions,
  ScrollView,
  StyleSheet,
  View
} from "react-native";
import { Icon, ThemeProvider } from "react-native-elements";
import FastImage from "react-native-fast-image";

export interface ProfileScreenProps extends MusicianView {
  componentId: string;
}

export default class ShowProfileView extends React.PureComponent<
  ProfileScreenProps
> {
  public static options() {
    return {
      topBar: {
        visible: false,
        animate: false
      }
    };
  }

  constructor(props) {
    super(props);
  }

  public render() {
    const { image, name, location, genres, roles, bio } = this.props;

    return (
      <ThemeProvider theme={theme}>
        <Animated.View style={styles.container}>
          <View style={styles.imageContainer}>
            <FastImage source={{ uri: image }} style={styles.image} />
          </View>

          <View style={{ flex: 1 }}>
            <ScrollView contentContainerStyle={styles.about}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ flex: 1 }}>
                  <Text type="h5" text={name} />
                  <Text
                    type="subtitle"
                    text={location}
                    style={{ marginTop: spacing.spacing1 }}
                  />
                </View>
                <Icon
                  name="message"
                  iconStyle={{ padding: spacing.spacing3 }}
                  onPress={this.messageUser}
                />
              </View>

              <View style={{ marginTop: spacing.spacing6 }}>
                <Text type="subtitle2" text="Genres" />
                <View style={{ marginTop: spacing.spacing2 }}>
                  <View style={styles.chips}>
                    {genres.map((genre, index) => (
                      <Chip
                        text={genre.charAt(0).toUpperCase() + genre.slice(1)}
                        style={styles.chip}
                        key={index}
                      />
                    ))}
                  </View>
                </View>
              </View>

              <View style={{ marginTop: spacing.spacing4 }}>
                <Text type="subtitle2" text="Roles" />
                <View style={{ marginTop: spacing.spacing2 }}>
                  <View style={styles.chips}>
                    {roles.map((role, index) => (
                      <Chip
                        text={role.charAt(0).toUpperCase() + role.slice(1)}
                        style={styles.chip}
                        key={index}
                      />
                    ))}
                  </View>
                </View>
              </View>

              <View style={{ marginTop: spacing.spacing4 }}>
                <Text type="subtitle2" text="Bio" />
                <Text
                  type="body"
                  text={bio}
                  style={{ marginTop: spacing.spacing1 }}
                />
              </View>
            </ScrollView>
          </View>
        </Animated.View>
      </ThemeProvider>
    );
  }

  private messageUser = () => {
    const { id, name, image } = this.props;
    openConversationWith(this.props.componentId, { id, name, image });
  };
}

const SCREEN_WIDTH = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  imageContainer: {
    width: SCREEN_WIDTH,
    height: SCREEN_WIDTH / 1.333
  },

  image: {
    width: undefined,
    height: undefined,
    flex: 1
  },

  subtitle: {
    color: colors.gray,
    fontSize: 14,
    letterSpacing: 0.25,
    lineHeight: 20
  },

  chips: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    marginHorizontal: -spacing.spacing2,
    marginTop: -spacing.spacing2
  },

  chip: {
    marginHorizontal: spacing.spacing2,
    marginTop: spacing.spacing2
  },

  about: {
    padding: spacing.spacing6
  }
});
