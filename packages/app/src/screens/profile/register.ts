import EditProfileScreen from "@screens/profile/EditProfileScreen";
import MeScreen from "@screens/profile/MeScreen";
import ProfileScreen from "@screens/profile/ProfileScreen";
import registerComponentWithProvider from "@screens/utils";
import { UserStore } from "@stores/index";
import { Navigation } from "react-native-navigation";

export default function(userStore: UserStore) {
  registerComponentWithProvider("profile.MeScreen", MeScreen, {
    userStore
  });

  registerComponentWithProvider(
    "profile.EditProfileScreen",
    EditProfileScreen,
    { userStore }
  );

  Navigation.registerComponent("profile.ProfileScreen", () => ProfileScreen);
}
