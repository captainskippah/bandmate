import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import { Genre, Role } from "@bandmate/types/musician";
import Chip from "@components/Chip";
import Text, { style } from "@components/Text";
import { UserStore } from "@stores/index";
import User from "@stores/user/User";
import { Formik, FormikActions, FormikProps } from "formik";
import React from "react";
import {
  Alert,
  BackHandler,
  ProgressBarAndroid,
  ScrollView,
  StyleSheet,
  ToastAndroid,
  View
} from "react-native";
import { Button, Icon, Input, ThemeProvider } from "react-native-elements";
import FastImage from "react-native-fast-image";
import ImagePicker from "react-native-image-picker";
import { Navigation } from "react-native-navigation";
import { goToHomeScreen } from "../layouts";

export interface EditProfileScreenProps {
  componentId: string;
  userStore: UserStore;
  firstTime: boolean;
}

interface EditProfileScreenState {
  saving: boolean;
}

export default class EditProfileScreen extends React.Component<
  EditProfileScreenProps,
  EditProfileScreenState
> {
  public static defaultProps = {
    firstTime: false
  };

  public static options(props) {
    return {
      bottomTabs: {
        animate: false,
        drawBehind: true,
        visible: false
      }
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      saving: false
    };
  }

  public render() {
    return (
      <Formik
        initialValues={this.props.userStore.getUser()!}
        isInitialValid={this.props.userStore.isRegistered()}
        validate={values => {
          const errors: { [key: string]: string } = {};

          if (values.name.trim() === "") {
            errors.name = "Cannot be empty";
          }

          if (values.bio.trim() === "") {
            errors.bio = "Cannot be empty";
          }

          if (values.roles.length === 0) {
            errors.roles = "At least 1 role must be selected";
          }

          if (values.genres.length === 0) {
            errors.genres = "At least 1 genre must be selected";
          }

          return errors;
        }}
        onSubmit={this.save}
        render={props => (
          <ThemeProvider theme={theme}>
            <View style={{ flex: 1 }}>
              <View
                style={{
                  height: 56,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                {this.props.firstTime ? null : (
                  <Icon
                    name="close"
                    color={colors.white}
                    iconStyle={{ padding: spacing.spacing2 }}
                    onPress={this.close}
                  />
                )}
                <Text
                  type="h6"
                  text="Edit Profile"
                  style={{ marginLeft: spacing.spacing2, color: colors.white }}
                />
                <Icon
                  name="check"
                  iconStyle={{ padding: spacing.spacing2 }}
                  containerStyle={{ marginLeft: "auto" }}
                  disabledStyle={{ backgroundColor: "transparent" }}
                  disabled={
                    !props.isValid || !props.dirty || props.isSubmitting
                  }
                  color={
                    props.isValid && props.dirty && !props.isSubmitting
                      ? colors.white
                      : colors.gray
                  }
                  onPress={props.submitForm}
                />
              </View>
              <ProgressBarAndroid
                animating={this.state.saving}
                indeterminate={true}
                styleAttr="Horizontal"
                color={colors.white}
              />
              <ScrollView
                style={{
                  flex: 1
                }}
                contentContainerStyle={{
                  paddingHorizontal: spacing.spacing4,
                  paddingVertical: spacing.spacing3
                }}
              >
                <View
                  style={{
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <FastImage
                    source={{ uri: props.values.image }}
                    style={{
                      width: 75,
                      height: 75,
                      borderRadius: 37.5
                    }}
                  />
                  <Button
                    title="Change Profile Photo"
                    containerStyle={{ marginTop: spacing.spacing2 }}
                    buttonStyle={{ backgroundColor: "transparent" }}
                    titleStyle={{ color: colors.blue }}
                    onPress={() =>
                      this.selectPhoto(props.handleChange("image"))
                    }
                  />
                </View>

                <Input
                  label={
                    props.errors.name ? `Name (${props.errors.name})` : "Name"
                  }
                  value={props.values.name}
                  labelStyle={{
                    ...style.subtitle2,
                    color: props.errors.name ? colors.red : colors.white
                  }}
                  inputContainerStyle={{ marginTop: spacing.spacing2 }}
                  onChangeText={props.handleChange("name")}
                  onBlur={props.handleBlur("name")}
                />

                <Input
                  label={props.errors.bio ? `Bio (${props.errors.bio})` : "Bio"}
                  value={props.values.bio}
                  multiline={true}
                  numberOfLines={4}
                  textAlignVertical="top"
                  labelStyle={{
                    ...style.subtitle2,
                    color: props.errors.bio ? colors.red : colors.white
                  }}
                  containerStyle={{
                    marginTop: spacing.spacing4,
                    minHeight: 112
                  }}
                  inputContainerStyle={{
                    marginTop: spacing.spacing2,
                    flex: 1
                  }}
                  onChangeText={props.handleChange("bio")}
                  onBlur={props.handleBlur("bio")}
                />

                <View style={{ marginTop: spacing.spacing4 }}>
                  <Text
                    type="subtitle2"
                    text={
                      props.errors.roles
                        ? `Roles (${props.errors.roles})`
                        : "Roles"
                    }
                    style={{
                      color: props.errors.roles ? colors.red : colors.white
                    }}
                  />
                  <View style={styles.tags}>
                    <Chip
                      text="Vocalist"
                      onPress={this.selectRole(props, "vocalist")}
                      highlighted={props.values.roles.includes("vocalist")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Guitarist"
                      onPress={this.selectRole(props, "guitarist")}
                      highlighted={props.values.roles.includes("guitarist")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Bassist"
                      onPress={this.selectRole(props, "bassist")}
                      highlighted={props.values.roles.includes("bassist")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Keyboardist"
                      onPress={this.selectRole(props, "keyboardist")}
                      highlighted={props.values.roles.includes("keyboardist")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Drummer"
                      onPress={this.selectRole(props, "drummer")}
                      highlighted={props.values.roles.includes("drummer")}
                      style={styles.tag}
                    />
                  </View>
                </View>

                <View style={{ marginTop: spacing.spacing4 }}>
                  <Text
                    type="subtitle2"
                    text={
                      props.errors.genres
                        ? `Genres (${props.errors.genres})`
                        : "Genres"
                    }
                    style={{
                      color: props.errors.genres ? colors.red : colors.white
                    }}
                  />
                  <View style={styles.tags}>
                    <Chip
                      text="Rock"
                      onPress={this.selectGenre(props, "rock")}
                      highlighted={props.values.genres.includes("rock")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Metal"
                      onPress={this.selectGenre(props, "metal")}
                      highlighted={props.values.genres.includes("metal")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Pop"
                      onPress={this.selectGenre(props, "pop")}
                      highlighted={props.values.genres.includes("pop")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Blues"
                      onPress={this.selectGenre(props, "blues")}
                      highlighted={props.values.genres.includes("blues")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Jazz"
                      onPress={this.selectGenre(props, "jazz")}
                      highlighted={props.values.genres.includes("jazz")}
                      style={styles.tag}
                    />
                    <Chip
                      text="Reggae / Ska"
                      onPress={this.selectGenre(props, "reggae")}
                      highlighted={props.values.genres.includes("reggae")}
                      style={styles.tag}
                    />
                  </View>
                </View>
              </ScrollView>
            </View>
          </ThemeProvider>
        )}
      />
    );
  }

  private close = () => {
    Navigation.popToRoot(this.props.componentId);
  };

  private save = async (values: User, actions: FormikActions<User>) => {
    if (this.state.saving) {
      return;
    }

    this.setState({ saving: true });

    try {
      await this.props.userStore.update(values);

      if (this.props.firstTime) {
        await goToHomeScreen();
      } else {
        ToastAndroid.show("Profile updated", ToastAndroid.SHORT);
        await Navigation.popToRoot(this.props.componentId);
      }
    } catch (e) {
      this.setState({ saving: true });

      if (this.props.firstTime) {
        Alert.alert(
          "Registration Failed",
          "Oopsie doopsie, something went wrong. I am working on getting this fixed as soon as I can. Please come back again later.",
          [
            {
              text: "I Understand",
              onPress() {
                BackHandler.exitApp();
              }
            }
          ]
        );
        await goToHomeScreen();
      } else {
        ToastAndroid.show(
          "Oopsie, something went wrong. Try again later.",
          ToastAndroid.SHORT
        );
        await Navigation.popToRoot(this.props.componentId);
      }
    }
  };

  private selectPhoto = (handler: (e: string) => void) => {
    ImagePicker.launchImageLibrary({}, response => {
      if (response.uri !== undefined) {
        handler(response.uri);
      }
    });
  };

  private selectRole = (formik: FormikProps<User>, role: Role) => {
    return () => {
      const roles = formik.values.roles;

      const newRoles = roles.includes(role)
        ? roles.filter(role2 => role2 !== role)
        : [...roles, role];

      // Sort values so value ordering does not matter
      // and thus will not become "dirty".
      formik.setFieldValue("roles", newRoles.sort());
    };
  };

  private selectGenre = (formik: FormikProps<User>, genre: Genre) => {
    return () => {
      const genres = formik.values.genres;

      const newGenres = genres.includes(genre)
        ? genres.filter(role2 => role2 !== genre)
        : [...genres, genre];

      // Sort values so value ordering does not matter
      // and thus will not become "dirty".
      formik.setFieldValue("genres", newGenres.sort());
    };
  };
}

const styles = StyleSheet.create({
  tags: {
    marginTop: spacing.spacing2,
    marginHorizontal: -spacing.spacing2,
    marginBottom: -spacing.spacing3,
    flexDirection: "row",
    flexWrap: "wrap"
  },

  tag: {
    marginHorizontal: spacing.spacing2,
    marginBottom: spacing.spacing3
  }
});
