import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import { Location } from "@bandmate/types/musician";
import Chip from "@components/Chip";
import Text from "@components/Text";
import { UserStore } from "@stores/index";
import Geocoder from "@utils/Geocoder";
import { reaction } from "mobx";
import { observer } from "mobx-react";
import React from "react";
import { Dimensions, ScrollView, StyleSheet, View } from "react-native";
import { Button, Icon, ThemeProvider } from "react-native-elements";
import FastImage from "react-native-fast-image";
import { Navigation } from "react-native-navigation";

export interface MeScreenProps {
  componentId: string;
  userStore: UserStore;
  logout: () => void;
}

@observer
export default class MeScreen extends React.PureComponent<
  MeScreenProps,
  { location: string }
> {
  public static options() {
    return {
      topBar: {
        visible: false,
        animate: false
      }
    };
  }

  private mounted;

  constructor(props) {
    super(props);

    this.mounted = false;

    this.state = {
      location: ""
    };
  }

  public componentDidMount() {
    this.mounted = true;

    reaction<Location | undefined>(
      () => this.props.userStore.getLocation(),
      userLocation => {
        if (userLocation === undefined) {
          this.setState({ location: "(Unknown Location)" });
        } else {
          Geocoder.geocodePosition(userLocation).then(location => {
            if (!this.mounted) {
              return;
            }
            this.setState({ location });
          });
        }
      },
      {
        name: "locationReaction",
        fireImmediately: true
      }
    );
  }

  public componentWillUnmount() {
    this.mounted = false;
  }

  public render() {
    if (this.props.userStore.getUser() === undefined) {
      return null;
    }

    const { image, name, genres, roles, bio } = this.props.userStore.getUser()!;

    return (
      <ThemeProvider theme={theme}>
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <FastImage source={{ uri: image }} style={styles.image} />
          </View>

          <View style={{ flex: 1 }}>
            <ScrollView contentContainerStyle={styles.about}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ flex: 1 }}>
                  <Text type="h5" text={name} />
                  <Text
                    type="subtitle"
                    text={this.state.location}
                    style={{ marginTop: spacing.spacing1 }}
                  />
                </View>
                <Icon
                  name="edit"
                  iconStyle={{ padding: spacing.spacing3 }}
                  onPress={this.edit}
                />
              </View>

              <View style={{ marginTop: spacing.spacing6 }}>
                <Text type="subtitle2" text="Genres" />
                <View style={{ marginTop: spacing.spacing2 }}>
                  <View style={styles.chips}>
                    {genres.map((genre, index) => (
                      <Chip
                        text={genre.charAt(0).toUpperCase() + genre.slice(1)}
                        style={styles.chip}
                        key={index}
                      />
                    ))}
                  </View>
                </View>
              </View>

              <View style={{ marginTop: spacing.spacing4 }}>
                <Text type="subtitle2" text="Roles" />
                <View style={{ marginTop: spacing.spacing2 }}>
                  <View style={styles.chips}>
                    {roles.map((role, index) => (
                      <Chip
                        text={role.charAt(0).toUpperCase() + role.slice(1)}
                        style={styles.chip}
                        key={index}
                      />
                    ))}
                  </View>
                </View>
              </View>

              <View style={{ marginTop: spacing.spacing4 }}>
                <Text type="subtitle2" text="Bio" />
                <Text
                  type="body"
                  text={bio}
                  style={{ marginTop: spacing.spacing1 }}
                />
              </View>

              <Button
                title="Logout"
                containerStyle={{
                  marginTop: spacing.spacing8
                }}
                buttonStyle={{
                  backgroundColor: "transparent",
                  borderWidth: 1,
                  borderColor: colors.white
                }}
                onPress={this.props.logout}
              />
            </ScrollView>
          </View>
        </View>
      </ThemeProvider>
    );
  }

  private edit = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "profile.EditProfileScreen"
      }
    });
  };
}

const SCREEN_WIDTH = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  imageContainer: {
    width: SCREEN_WIDTH,
    height: SCREEN_WIDTH / 1.333
  },

  image: {
    width: undefined,
    height: undefined,
    flex: 1
  },

  subtitle: {
    color: colors.gray,
    fontSize: 14,
    letterSpacing: 0.25,
    lineHeight: 20
  },

  chips: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: -spacing.spacing2,
    marginTop: -spacing.spacing2
  },

  chip: {
    marginHorizontal: spacing.spacing2,
    marginTop: spacing.spacing2
  },

  about: {
    padding: spacing.spacing6
  }
});
