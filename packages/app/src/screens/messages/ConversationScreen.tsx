import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import Loading from "@components/Loading";
import Text from "@components/Text";
import { Message, User } from "@core/messages/types";
import { MessagesStore } from "@stores/index";
import Conversation from "@stores/messages/Conversation";
import { computed } from "mobx";
import { observer } from "mobx-react";
import React, { RefObject } from "react";
import { ActivityIndicator, FlatList, ListRenderItem, TextInput, View, ViewToken } from "react-native";
import { Icon, ThemeProvider } from "react-native-elements";
import FastImage from "react-native-fast-image";
import { Navigation } from "react-native-navigation";

interface MessageViewProps {
  message: string;
  fromSender: boolean;
  prevFromSame: boolean;
}

const MessageView = React.memo(
  ({ message, fromSender, prevFromSame }: MessageViewProps) => (
    <View
      style={{
        paddingHorizontal: spacing.spacing4,
        marginTop: prevFromSame ? spacing.spacing2 : spacing.spacing3,
        flexDirection: "row",
        justifyContent: fromSender ? "flex-end" : "flex-start"
      }}
    >
      <Text
        type="body"
        text={message}
        style={{
          maxWidth: "75%",
          paddingVertical: spacing.spacing2,
          paddingHorizontal: spacing.spacing3,
          borderRadius: 12,
          backgroundColor: colors.black,
          flexWrap: "wrap",
          alignSelf: "flex-start"
        }}
      />
    </View>
  )
);

export interface ConversationScreenProps {
  messagesStore: MessagesStore;
  recipient: User;
}

interface ConversationScreenState {
  loading: boolean;
  conversation: Conversation | undefined;
  listRef: RefObject<FlatList<Message>>;
  message: string;
}

@observer
export default class ConversationScreen extends React.Component<
  ConversationScreenProps,
  ConversationScreenState
  > {
  public static options(props: ConversationScreenProps) {
    return {
      bottomTabs: {
        animate: false,
        drawBehind: true,
        visible: false
      }
    };
  }

  private mounted: boolean;

  @computed
  private get messages() {
    return this.state.conversation === undefined
      ? []
      : this.state.conversation.getMessages().reverse();
  }

  constructor(props: ConversationScreenProps) {
    super(props);

    this.mounted = false;

    this.state = {
      loading: false,
      conversation: undefined,
      listRef: React.createRef(),
      message: ""
    };
  }

  public componentDidMount() {
    this.mounted = true;

    this.setState({
      loading: true
    });

    this.props.messagesStore
      .getConversationWith(this.props.recipient.id)
      .then(conversation => {
        if (!this.mounted) {
          return;
        }

        this.setState({
          conversation,
          loading: false
        });
      });
  }

  public componentWillUnmount() {
    this.mounted = false;
  }

  public render() {
    const recipient = this.props.recipient;

    return (
      <ThemeProvider theme={theme}>
        <View style={{ flex: 1 }}>
          <View
            style={{
              height: 56,
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Icon
              name="arrow-back"
              color={colors.white}
              iconStyle={{ padding: spacing.spacing2 }}
              onPress={this.goBack}
            />
            <FastImage
              source={{ uri: recipient.image }}
              style={{
                width: 34,
                height: 34,
                borderRadius: 17,
                marginLeft: spacing.spacing2
              }}
            />
            <Text
              type="subtitle"
              text={recipient.name}
              style={{ marginLeft: spacing.spacing4 }}
            />
          </View>
          {this.state.loading ? (
            <Loading />
          ) : (
            <FlatList
              ref={this.state.listRef}
              data={this.messages}
              keyExtractor={message => message.id}
              initialNumToRender={20} // max initial messages length
              renderItem={this.messageListItem}
              inverted={true}
              onScroll={this.onScroll}
              scrollEventThrottle={100}
              onScrollToIndexFailed={() => null}
              viewabilityConfig={{
                minimumViewTime: 200,
                itemVisiblePercentThreshold: 80
              }}
              onViewableItemsChanged={this.onMessagesVisibleChanged}
              contentContainerStyle={{
                paddingTop: spacing.spacing2
              }}
              ListFooterComponent={
                <ActivityIndicator
                  animating={this.state.conversation?.isLoadingPrevious() || false}
                  style={{
                    height: this.state.conversation?.hasMorePrevious ? undefined : 0
                  }}
                  color={colors.blue}
                  size="large"
                />
              }
            />
          )}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              placeholder="Type a message..."
              placeholderTextColor={colors.gray}
              returnKeyType="none"
              multiline={true}
              value={this.state.message}
              onChangeText={message => this.setState({ message })}
              style={{
                color: colors.white,
                flex: 1,
                paddingHorizontal: spacing.spacing4
              }}
            />
            <Icon
              name="send"
              color={this.canSendMessage() ? colors.white : colors.gray}
              iconStyle={{ padding: spacing.spacing3 }}
              containerStyle={{ alignSelf: "flex-end" }}
              disabledStyle={{ backgroundColor: "transparent" }}
              disabled={!this.canSendMessage()}
              onPress={this.sendMessage}
            />
          </View>
        </View>
      </ThemeProvider>
    );
  }

  private goBack = () => {
    // @ts-ignore
    Navigation.popToRoot(this.props.componentId);
  };

  private scrollToBottom = () => {
    if (this.messages.length === 0) {
      return;
    }

    this.state.listRef.current!.scrollToIndex({
      index: 0,
      animated: false,
      viewOffset: spacing.spacing2
    })
  };

  private onScroll = ({ nativeEvent }) => {
    if (this.state.conversation === undefined || !this.state.conversation.hasMorePrevious) {
      return;
    }

    const onTop =
      nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
      nativeEvent.contentSize.height - spacing.spacing2;

    if (onTop) {
      this.state.conversation.loadPrevious();
    }
  };

  private onMessagesVisibleChanged = ({ viewableItems }: { viewableItems: ViewToken[] }) => {
    if (viewableItems.length === 0 || this.state.conversation === undefined) {
      return;
    }

    const lastVisibleMessage = viewableItems[0];

    if (lastVisibleMessage.index === 0) {
      this.state.conversation.seen();
    }
  };

  private messageListItem: ListRenderItem<Message> = ({ item, index }) => {
    const prevFromSame =
      index === 0 ? true : this.messages[index - 1].sender === item.sender;

    return (
      <MessageView
        message={item.message}
        fromSender={item.sender === this.props.messagesStore.getUserId()!}
        prevFromSame={prevFromSame}
      />
    );
  };

  private canSendMessage = () => this.state.message.trim() !== "";

  private sendMessage = () => {
    if (!this.canSendMessage()) {
      return;
    }

    let conversation = this.state.conversation;
    let createdNew = false;

    if (conversation === undefined) {
      const { id, name, image } = this.props.recipient;

      conversation = this.props.messagesStore.startConversationWith({
        id,
        name,
        image
      });

      createdNew = true;
    }

    conversation.sendMessage(this.state.message);

    if (createdNew) {
      this.setState({ conversation, message: "" });
    } else {
      this.setState({ message: "" });
    }

    this.scrollToBottom();
  };
}
