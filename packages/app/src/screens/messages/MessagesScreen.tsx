import openConversationWith from "@actions/openConversationWith";
import colors from "@assets/colors";
import spacing from "@assets/spacing";
import theme from "@assets/theme";
import Text from "@components/Text";
import { User } from "@core/messages/types";
import { MessagesStore } from "@stores/index";
import Conversation from "@stores/messages/Conversation";
import dFormat from "date-fns/format";
import isToday from "date-fns/isToday";
import { when } from "mobx";
import { Observer, observer } from "mobx-react";
import React, { Component } from "react";
import { FlatList, ListRenderItem, RefreshControl, View } from "react-native";
import { ListItem, ThemeProvider } from "react-native-elements";
import FastImage from "react-native-fast-image";

interface MessagesScreenState {
  loading: boolean;
}

export interface MessagesScreenProps {
  componentId: string;
  messagesStore: MessagesStore;
}

function format(date: Date): string {
  return isToday(date) ? dFormat(date, "hh:mm a") : dFormat(date, "MMM d");
}

@observer
export default class MessagesScreen extends Component<
  MessagesScreenProps,
  MessagesScreenState
> {
  public static options() {
    return {
      topBar: {
        visible: false
      },
      bottomTabs: {
        visible: true
      }
    };
  }

  private mounted: boolean;

  constructor(props) {
    super(props);

    this.mounted = false;

    this.state = {
      loading: true
    };
  }

  public componentDidMount() {
    this.mounted = true;

    when(
      () => this.props.messagesStore.isInitialized(),
      () => {
        if (!this.mounted) {
          return;
        }
        this.setState({ loading: false });
      }
    );
  }

  public componentWillUnmount() {
    this.mounted = false;
  }

  public render() {
    const conversations = this.props.messagesStore
      .getConversations()
      .sort((a, b) => {
        const aLast = a.getLastMessage();
        const bLast = b.getLastMessage();

        if (aLast === undefined && bLast === undefined) {
          return 0;
        }

        if (aLast === undefined) {
          return -1;
        }

        if (bLast === undefined) {
          return 1;
        }

        return bLast.date.getTime() - aLast.date.getTime();
      });

    return (
      <ThemeProvider theme={theme}>
        <View style={{ flex: 1 }}>
          <View
            style={{
              height: 56,
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Text
              type="h6"
              text="Messages"
              style={{ marginLeft: spacing.spacing4 }}
            />
          </View>

          <FlatList
            contentContainerStyle={{
              paddingVertical: spacing.spacing3,
              flex: conversations.length === 0 ? 1 : 0 // FlatList is not scrollable when flex is set.
            }}
            keyExtractor={conversation => conversation.getId()}
            data={conversations}
            renderItem={this.conversationListItem}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                enabled={false}
                colors={[colors.blue]}
              />
            }
            ListEmptyComponent={
              <View
                style={{
                  flex: 1,
                  height: "100%",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text
                  type="body"
                  text="You have no messages right now"
                  style={{ color: colors.gray }}
                />
              </View>
            }
          />
        </View>
      </ThemeProvider>
    );
  }

  private conversationListItem: ListRenderItem<Conversation> = ({ item }) => {
    return (
      <Observer>
        {() => {
          const { name, image } = item.getRecipient();
          const lastMessage = item.getLastMessage();
          let formatted = "(No Message)";

          if (lastMessage !== undefined) {
            const { message, date } = lastMessage;
            formatted = `${message} ${String.fromCharCode(183)} ${format(
              date
            )}`;
          }

          return (
            <ListItem
              leftAvatar={
                <FastImage
                  style={{
                    width: 50,
                    height: 50,
                    borderRadius: 25
                  }}
                  source={{ uri: image }}
                />
              }
              title={name}
              subtitle={formatted}
              subtitleProps={{ numberOfLines: 2 }}
              titleStyle={{
                fontWeight: item.unreadCount > 0 ? "700" : "500"
              }}
              subtitleStyle={{
                color: item.unreadCount > 0 ? colors.white : colors.gray,
                fontWeight: item.unreadCount > 0 ? "700" : "400"
              }}
              onPress={() => this.openConversation(item.getRecipient())}
            />
          );
        }}
      </Observer>
    );
  };

  private openConversation = (recipient: User) => {
    openConversationWith(this.props.componentId, recipient);
  };
}
