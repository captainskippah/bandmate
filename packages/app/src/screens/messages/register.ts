import registerComponentWithProvider from "@screens/utils";
import { MessagesStore } from "@stores/index";
import ConversationScreen from "./ConversationScreen";
import MessagesScreen from "./MessagesScreen";

export default function(messagesStore: MessagesStore) {
  registerComponentWithProvider("messages.MessagesScreen", MessagesScreen, {
    messagesStore
  });

  registerComponentWithProvider(
    "messages.ConversationScreen",
    ConversationScreen,
    {
      messagesStore
    }
  );
}
