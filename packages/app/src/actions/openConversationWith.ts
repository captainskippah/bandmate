import { User } from "@core/messages/types";
import { Navigation } from "react-native-navigation";
import { ConversationScreenProps } from "src/screens/messages/ConversationScreen";

export default function(componentId: string, recipient: User) {
  // ConversationScreen is registered with MessagesStore so we dont have to pass it ourselves
  Navigation.push<Omit<ConversationScreenProps, "messagesStore">>(componentId, {
    component: {
      id: "messages.ConversationScreen",
      name: "messages.ConversationScreen",
      passProps: {
        recipient
      }
    }
  });
}
