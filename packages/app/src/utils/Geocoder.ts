import { Location } from "@bandmate/types/musician";
import { NativeModules } from "react-native";

const geocoder = NativeModules.RNGeocoder;

export default {
  async geocodePosition(location: Location): Promise<string> {
    const places = await geocoder.geocodePosition(location);

    let place = null;

    places.forEach(_place => {
      if (place === null) {
        place = _place.locality;
      }
    });

    if (place === null) {
      places.forEach(_place => {
        if (place === null) {
          place = _place.subAdminArea;
        }
      });
    }

    return place!;
  }
};
