import { Location } from "@bandmate/types/musician";
import { NativeModules } from "react-native";

export default {
  distanceBetween(location: Location, location2: Location) {
    return NativeModules.RNLocationUtil.distanceBetween(location, location2);
  }
};
