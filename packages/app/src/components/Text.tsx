import React from "react";
import { StyleProp, StyleSheet, TextStyle } from "react-native";
import { Text, withTheme } from "react-native-elements";

export const style = StyleSheet.create({
  h1: {
    fontSize: 96,
    fontWeight: "400",
    letterSpacing: -1.5
  },

  h2: {
    fontSize: 60,
    fontWeight: "400",
    letterSpacing: -0.5
  },

  h3: {
    fontSize: 48,
    fontWeight: "normal",
    letterSpacing: 0
  },

  h4: {
    fontSize: 34,
    fontWeight: "normal",
    letterSpacing: 0.25
  },

  h5: {
    fontSize: 24,
    fontWeight: "normal",
    letterSpacing: 0
  },

  h6: {
    fontSize: 20,
    fontWeight: "bold",
    letterSpacing: 0.15
  },

  subtitle: {
    fontSize: 16,
    fontWeight: "normal",
    letterSpacing: 0.15
  },

  subtitle2: {
    fontSize: 14,
    fontWeight: "bold",
    letterSpacing: 0.1
  },

  body: {
    fontSize: 16,
    fontWeight: "normal",
    letterSpacing: 0.5,
    lineHeight: 24
  },

  body2: {
    fontSize: 14,
    fontWeight: "normal",
    letterSpacing: 0.25,
    lineHeight: 20
  },

  caption: {
    fontSize: 12,
    fontWeight: "normal",
    letterSpacing: 0.4
  }
});

interface TextProps {
  text: string;
  align?: "left" | "center" | "right";
  type:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "subtitle"
    | "subtitle2"
    | "body"
    | "body2"
    | "caption";
  style?: StyleProp<TextStyle>;
}

export default withTheme<TextProps, TextStyle>(
  ({ text, type, theme, style: propStyle }) => {
    const themeStyle = (theme.Text || {}).style;
    return <Text style={[themeStyle, style[type], propStyle]}>{text}</Text>;
  }
);
