import React from "react";
import { StyleSheet, ViewStyle } from "react-native";
import LinearGradient from "react-native-linear-gradient";

const style = StyleSheet.create({
  scrim: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }
});

interface ScrimProps {
  style?: ViewStyle;
}

const ease = {
  colors: [
    "hsla(0, 0%, 0%, 0)",
    "hsla(0, 0%, 0%, 0.011)",
    "hsla(0, 0%, 0%, 0.042)",
    "hsla(0, 0%, 0%, 0.089)",

    "hsla(0, 0%, 0%, 0.147)",
    "hsla(0, 0%, 0%, 0.213)",
    "hsla(0, 0%, 0%, 0.283)",
    "hsla(0, 0%, 0%, 0.357)",

    "hsla(0, 0%, 0%, 0.433)",
    "hsla(0, 0%, 0%, 0.511)",
    "hsla(0, 0%, 0%, 0.591)",
    "hsla(0, 0%, 0%, 0.672)",
    "hsla(0, 0%, 0%, 0.753)",
    "hsla(0, 0%, 0%, 0.834)",
    // "hsla(0, 0%, 0%, 0.917)",
    // "hsl(0, 0%, 0%)"
  ],
  location: [
    0,
    0.094,
    0.186,
    0.271,
    0.35,
    0.423,
    0.491,
    0.555,
    0.616,
    0.675,
    0.732,
    0.787,
    0.841,
    // 0.894,
    // 0.947,
    1
  ],
  start: {
    x: 0,
    y: 0
  },
  end: {
    x: 0,
    y: 1
  }
};

const scrim = {
  colors: [
    "hsla(0, 0%, 0%, 0.002)",
    "hsla(0, 0%, 0%, 0.008)",
    "hsla(0, 0%, 0%, 0.021)",
    "hsla(0, 0%, 0%, 0.042)",
    "hsla(0, 0%, 0%, 0.075)",
    "hsla(0, 0%, 0%, 0.126)",
    "hsla(0, 0%, 0%, 0.194)",
    "hsla(0, 0%, 0%, 0.278)",
    "hsla(0, 0%, 0%, 0.382)",
    "hsla(0, 0%, 0%, 0.541)",
    "hsla(0, 0%, 0%, 0.738)",
    "hsl(0, 0%, 0%)"
  ],
  location: [
    0,
    0.018,
    0.048,
    0.09,
    0.139,
    0.198,
    0.27,
    0.35,
    0.435,
    0.53,
    0.66,
    // 0.81,
    1
  ]
};

export default function(props: React.PropsWithChildren<ScrimProps>) {
  return (
    <LinearGradient {...ease} style={[style.scrim, props.style]}>
      {props.children}
    </LinearGradient>
  );
}
