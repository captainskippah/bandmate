import colors from "@assets/colors";
import spacing from "@assets/spacing";
import React from "react";
import { ActivityIndicator, View } from "react-native";
import { Text } from "react-native-elements";

export default function() {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <ActivityIndicator size="large" color={colors.white} />
      <Text style={{ color: colors.white, marginTop: spacing.spacing6 }}>
        Loading...
      </Text>
    </View>
  );
}
