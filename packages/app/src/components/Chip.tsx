import colors from "@assets/colors";
import spacing from "@assets/spacing";
import Text from "@components/Text";
import React from "react";
import { TouchableOpacity, View, ViewStyle } from "react-native";

interface ChipProps {
  text: string;
  onPress?: () => void;
  highlighted?: boolean;
  style?: ViewStyle;
}

export default React.memo<ChipProps>(
  ({ text, onPress, highlighted = false, style }: ChipProps) => {
    return onPress ? (
      <TouchableOpacity
        onPress={onPress}
        style={[
          {
            backgroundColor: highlighted ? colors.white : "transparent",
            borderColor: colors.gray,
            borderWidth: 1,
            borderRadius: 2,
            paddingHorizontal: spacing.spacing4,
            height: 36,
            minWidth: 64,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          },
          style
        ]}
      >
        <Text
          type="body2"
          text={text}
          style={{ color: highlighted ? colors.black : colors.white }}
        />
      </TouchableOpacity>
    ): (
      <View
        style={[
          {
            backgroundColor: highlighted ? colors.white : "transparent",
            borderColor: colors.gray,
            borderWidth: 1,
            borderRadius: 2,
            paddingHorizontal: spacing.spacing4,
            height: 36,
            minWidth: 64,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          },
          style
        ]}
      >
        <Text
          type="body2"
          text={text}
          style={{ color: highlighted ? colors.black : colors.white }}
        />
      </View>
    )
  }
);
