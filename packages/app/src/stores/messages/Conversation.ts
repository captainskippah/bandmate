import MessageService from "@core/messages/MessageService";
import { ConversationMeta, Message, User } from "@core/messages/types";
import { action, computed, observable, runInAction } from "mobx";
import uuid from "uuid";

export default class Conversation {
  private readonly messageService: MessageService;
  private readonly id: string;
  private readonly recipient: User;

  @observable
  private firstMessageId: string | undefined;

  @observable
  private lastSeenMessageId: string | undefined;

  @observable
  private messages: Map<string, Message>;

  @observable
  private loadingPrevious: boolean;

  @computed
  public get hasMorePrevious() {
    if (this.firstMessageId === undefined) {
      return false;
    }

    return !this.messages.has(this.firstMessageId);
  }

  @computed
  public get unreadCount() {
    let unreadCount = 0;

    // We can start counting if there's no lastSeenMessageId
    // Usually when new Conversation comes in.
    let startCounting = this.lastSeenMessageId === undefined;

    this.messages.forEach(message => {
      // We can't start counting because we have lastSeenMessageId
      if (!startCounting) {
        // We can start counting messages AFTER lastSeenMessageId
        if (message.id === this.lastSeenMessageId) {
          startCounting = true;
        }
        return;
      }

      if (message.sender === this.recipient.id) {
        unreadCount += 1;
      } else {
        // Maybe a network failure happened and we weren't able to send our last seen.
        // If we sent a message, then previous messages were seen already.
        // In that case we reset counter to 0 and count only messages after our replies.
        unreadCount = 0;
      }
    });

    return unreadCount;
  }

  constructor(
    messageService: MessageService,
    id = uuid.v4(),
    recipient: User,
    meta: ConversationMeta = {}
  ) {
    this.messageService = messageService;
    this.id = id;
    this.recipient = recipient;
    this.firstMessageId = meta.firstMessageId;
    this.lastSeenMessageId = meta.lastSeenMessageId;
    this.messages = new Map();
    this.loadingPrevious = false;
  }

  public getId(): string {
    return this.id;
  }

  public getRecipient(): User {
    return this.recipient;
  }

  public getMessages(): Message[] {
    return Array.from(this.messages.values());
  }

  public getLastMessage(): Message | undefined {
    const messages = this.getMessages();
    return messages[messages.length - 1];
  }

  public isLoadingPrevious(): boolean {
    return this.loadingPrevious;
  }

  @action
  public sendMessage(message: string): void {
    const messageModel = {
      message,
      id: uuid.v4(),
      date: new Date(),
      sender: this.messageService.getUserId()!
    };

    // We update the UI right away
    // instead of waiting for service to successfully send message
    // because we dont want a pause in UI.
    this.messages.set(messageModel.id, messageModel);

    if (this.messages.size === 1) {
      this.firstMessageId = messageModel.id;
    }

    this.messageService.sendMessage(this.id, messageModel);
  }

  @action
  public receiveMessage(message: Message) {
    if (this.messages.has(message.id)) {
      console.warn(
        "Received a message that we already have. Still receiving it because this should be an idempotent operation"
      );
    }

    this.messages.set(message.id, message);
  }

  @action
  public seen() {
    const lastMessage = this.getLastMessage();

    if (
      lastMessage === undefined ||
      lastMessage.id === this.lastSeenMessageId ||
      lastMessage.sender !== this.recipient.id
    ) {
      return;
    }

    this.lastSeenMessageId = lastMessage.id;
    this.messageService.seen(this.id, lastMessage.id);
  }

  @action
  public async loadPrevious() {
    if (this.loadingPrevious || !this.hasMorePrevious) {
      return;
    }

    this.loadingPrevious = true;

    const messages = await this.messageService.getMessages(
      this.id,
      this.messages.keys().next().value
    );

    const messageMap = new Map();

    messages.forEach(message => {
      messageMap.set(message.id, message);
    });

    runInAction(() => {
      this.messages = new Map([...messageMap, ...this.messages]);
      this.loadingPrevious = false;
    });
  }
}
