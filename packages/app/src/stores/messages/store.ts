import MessageService from "@core/messages/MessageService";
import { ConversationMeta, User } from "@core/messages/types";
import crashlytics from "@react-native-firebase/crashlytics";
import Conversation from "@stores/messages/Conversation";
import { action, computed, observable, runInAction } from "mobx";
import remotedev from "mobx-remotedev";
import uuid from "uuid";

@remotedev({ name: "Messages Store" })
export default class MessagesStore {
  private readonly messageService: MessageService;

  @observable
  private initialized: boolean;

  @observable
  private readonly conversations: Map<string, Conversation>;

  @computed
  public get unreadCount() {
    let count = 0;

    this.conversations.forEach(conversation => {
      count += conversation.unreadCount;
    });

    return count;
  }

  constructor(messageService: MessageService) {
    this.messageService = messageService;
    this.initialized = false;
    this.conversations = new Map();
  }

  @action
  public async initialize(): Promise<void> {
    if (this.initialized) {
      throw new Error("MessagesStore: already initialized!");
    }

    console.log("MessagesStore: will now initialize");

    try {
      await this.messageService.initialize();

      this.messageService.registerListeners({
        onConversationReceived: (
          conversationId: string,
          recipient: User,
          meta: ConversationMeta
        ) => {
          runInAction(() => {
            this.conversations.set(
              conversationId,
              new Conversation(
                this.messageService,
                conversationId,
                recipient,
                meta
              )
            );
          });
        },
        onMessageReceived: (conversationId, message) => {
          const conversation = this.conversations.get(conversationId);

          if (conversation === undefined) {
            console.warn(
              "Message received but Conversation is not in MessageStore"
            );
            return;
          }

          conversation.receiveMessage(message);
        }
      });

      await this.messageService.receiveConversations();

      runInAction(() => {
        this.initialized = true;
      });

      console.log("MessagesStore: initialization done");
    } catch (e) {
      if (__DEV__) {
        console.error("MessageStore: initialization failed", e);
      } else {
        crashlytics().log("MessageStore: initialization failed");
        crashlytics().recordError(e);
      }
      throw e;
    }
  }

  public isInitialized() {
    return this.initialized;
  }

  @action
  public dispose() {
    this.conversations.clear();
    this.initialized = false;
  }

  @action
  public startConversationWith(recipient: User): Conversation {
    for (const [id, conversation] of this.conversations) {
      if (conversation.getRecipient().id === recipient.id) {
        return this.conversations.get(id)!;
      }
    }

    const newConversation = new Conversation(
      this.messageService,
      uuid.v4(),
      recipient
    );

    this.conversations.set(newConversation.getId(), newConversation);

    this.messageService.createConversation(
      newConversation.getId(),
      recipient.id
    );

    return newConversation;
  }

  public getConversations(): Conversation[] {
    return Array.from(this.conversations.values());
  }

  public getConversation(id: string): Conversation | undefined {
    return this.conversations.get(id);
  }

  public async getConversationWith(
    userId: string
  ): Promise<Conversation | undefined> {
    for (const [id, conversation] of this.conversations) {
      if (conversation.getRecipient().id === userId) {
        return this.conversations.get(id);
      }
    }

    // TODO: attempt looking on server if not found locally because we paginate
    // if not, it'll look like we are creating new conversation.
    // ChatKit, our current implementation, does not paginate and so we dont need extra logic currently.

    return undefined;
  }

  public getUserId(): string {
    return this.messageService.getUserId();
  }
}
