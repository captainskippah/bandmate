import { Genre, Location, Musician, Role } from "@bandmate/types/musician";
import { observable } from "mobx";

/**
 * Represents the currently logged in Musician
 */
export default class User implements Musician {
  public readonly id: string;

  @observable
  public name: string;

  @observable
  public image: string;

  @observable
  public genres: Genre[];

  @observable
  public roles: Role[];

  @observable
  public bio: string;

  @observable
  public location?: Location;

  constructor(
    id: string,
    name: string,
    image: string,
    genres: Genre[],
    roles: Role[],
    bio: string
  ) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.genres = genres.slice().sort();
    this.roles = roles.slice().sort();
    this.bio = bio;
  }
}
