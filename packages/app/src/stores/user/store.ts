import { Location, Musician } from "@bandmate/types/musician";
import AuthService from "@core/auth/AuthService";
import MusicianService from "@core/musician/MusicianService";
import crashlytics from "@react-native-firebase/crashlytics";
import storage from "@react-native-firebase/storage";
import User from "@stores/user/User";
import { action, computed, observable, runInAction, toJS } from "mobx";
import remotedev from "mobx-remotedev";
import { PermissionsAndroid } from "react-native";
import Geolocation from "react-native-geolocation-service";

@remotedev({ name: "User Store" })
export default class UserStore {
  @observable
  public locationError: Geolocation.PositionError | undefined;

  private readonly authService: AuthService;

  private readonly musicianService: MusicianService;

  private initialized: boolean;

  @observable
  private registered: boolean;

  @observable
  private user: Musician | undefined;

  @computed
  public get isAuthenticated() {
    return this.user !== undefined;
  }

  constructor(authService: AuthService, musicianService: MusicianService) {
    this.authService = authService;
    this.musicianService = musicianService;
    this.initialized = false;
    this.registered = false;
  }

  @action
  public async initialize(): Promise<void> {
    if (this.initialized) {
      throw new Error("UserStore: already initialized!");
    }

    console.log("UserStore: will now initialize");

    try {
      const user = this.authService.getUser();

      if (user !== undefined) {
        const musician = await this.musicianService.getMusician();

        runInAction(() => {
          this.registered = musician !== undefined;
          this.user = {
            ...user,
            bio: musician?.bio || "",
            genres: musician?.genres || [],
            roles: musician?.roles || []
          };
        });

        // We don't want to halt app on location error.
        // We can just ask for permission again later.
        // tslint:disable-next-line:no-empty
        await this.initializeLocation().catch(() => {});
      }
    } catch (e) {
      if (__DEV__) {
        console.error("UserStore: initialization failed", e);
      } else {
        crashlytics().log("UserStore: initialization failed");
        crashlytics().recordError(e);
      }
      throw e;
    }

    runInAction(() => {
      this.initialized = true;
    });

    console.log("UserStore: initialization done");
  }

  @action
  public dispose() {
    console.log("UserStore: disposing.");

    this.initialized = false;
    this.user = undefined;
    this.registered = false;
    this.locationError = undefined;
    Geolocation.stopObserving();

    console.log("UserStore: disposed.");
  }

  public initializeLocation(): Promise<void> {
    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    ).then(permission => {
      if (permission !== PermissionsAndroid.RESULTS.GRANTED) {
        this.locationError = Geolocation.PositionError.PERMISSION_DENIED;
        return Promise.reject(Geolocation.PositionError.PERMISSION_DENIED);
      }

      return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(
          async position => {
            console.log("Current user position:", position);

            // Our initial request might have been denied
            // and was granted now on this request.
            // Clear the location error.
            this.locationError = undefined;

            await this.updateLocation({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            });

            resolve();

            Geolocation.watchPosition(
              _position => {
                console.log("Geolocation watcher triggered");
                this.updateLocation({
                  latitude: _position.coords.latitude,
                  longitude: _position.coords.longitude
                });
              },
              error => {
                if (__DEV__) {
                  console.error("Geolocation watcher error", error);
                } else {
                  const error2 = new Error("GeoLocation watchPosition error");
                  crashlytics().recordError(Object.assign(error2, error));
                }
              },
              {
                distanceFilter: 10,
                useSignificantChanges: true
              }
            );
          },
          error => {
            console.warn("getCurrentPosition error", error);
            this.locationError = error.code;
            reject();
          },
          {
            timeout: 5000,
            distanceFilter: 10,
            useSignificantChanges: true
          }
        );
      });
    });
  }

  public async logout(): Promise<void> {
    await this.authService.logout();
  }

  @action
  public async update(user: Omit<User, "id">): Promise<void> {
    if (this.user === undefined) {
      console.warn("UserStore.update called while user is undefined");
      return;
    }

    if (this.user.image !== user.image) {
      const imageRef = await storage()
        .ref(`users/${this.user.id}/image`);

      try {
        await imageRef.putFile(user.image);
      } catch (e) {
        if (__DEV__) {
          console.warn("UserStore.update: failed uploading image.", e)
        } else {
          crashlytics().log("UserStore.update: failed uploading image.");
          crashlytics().recordError(e);
        }
      }

      try {
        user.image = await imageRef.getDownloadURL()
      } catch (e) {
        if (__DEV__) {
          console.warn("UserStore.update: failed retrieving uploaded image URL..", e)
        } else {
          crashlytics().log("UserStore.update: failed retrieving uploaded image URL..");
          crashlytics().recordError(e);
        }
      }
    }

    user.bio = user.bio.trim();

    await this.musicianService!.updateMusician({
      ...user,
      id: this.user.id
    });

    runInAction(() => Object.assign(this.user, user));
  }

  @action
  public async updateLocation(location: Location) {
    if (this.user === undefined) {
      console.warn("UserStore.updateLocation called while user is undefined");
      return;
    }

    this.user.location = location;

    // Cannot update non-existing record on our server.
    // UserStore.update can be used instead (which will be called on registration)
    if (!this.isRegistered()) {
      return;
    }

    await this.musicianService!.updateLocation(location);
  }

  public getUser(): User | undefined {
    // return non-observable object because mutations should be done thru UserStore methods
    return toJS(this.user);
  }

  // Just a convenience method
  public getLocation(): Location | undefined {
    const user = this.getUser();

    if (user === undefined) {
      console.warn("UserStore.getLocation called while user is undefined");
      return undefined;
    }

    return user.location;
  }

  public isRegistered(): boolean {
    return toJS(this.registered);
  }
}
