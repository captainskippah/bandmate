export default {
  spacing1: 4,
  spacing2: 8,
  spacing3: 12,
  spacing4: 16,
  spacing5: 20,
  spacing6: 24,
  spacing7: 28,
  spacing8: 32,
  spacing9: 36,
  spacing10: 40
};
