import colors from "./colors";
import spacing from "./spacing";

export default {
  Input: {
    placeholderTextColor: colors.gray,
    containerStyle: {
      paddingLeft: 0,
      paddingRight: 0
    },
    inputContainerStyle: {
      borderRadius: 8,
      borderBottomWidth: 0,

      height: 48,
      paddingVertical: spacing.spacing2,
      paddingHorizontal: spacing.spacing3,

      backgroundColor: colors.black
    },
    inputStyle: {
      color: colors.white,
      paddingTop: 0,
      paddingBottom: 0
    }
  },
  Text: {
    style: {
      color: colors.white,
      fontSize: 14,
      letterSpacing: 0.5
    }
  },
  Icon: {
    color: colors.white
  },
  ListItem: {
    pad: 16,
    containerStyle: {
      height: 72,
      paddingHorizontal: spacing.spacing4,
      backgroundColor: "transparent"
    },
    titleStyle: {
      fontSize: 14
    },
    subtitleStyle: {
      fontSize: 14,
      color: colors.gray
    },
    rightSubtitleStyle: {
      color: colors.gray
    }
  }
};
