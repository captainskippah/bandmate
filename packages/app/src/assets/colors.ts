export default {
  blacker: "hsl(0, 0%, 5%)",
  black: "hsl(0, 0%, 13%)",
  gray: "hsl(0, 0%, 63%)",
  white: "hsl(0, 0%, 100%)",
  teal: "hsl(144, 89%, 55%)",
  red: "hsl(8, 89%, 55%)",
  blue: "hsl(207, 89%, 55%)"
};
