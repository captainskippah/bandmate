// reflect-metadata needs to be at the top
import "reflect-metadata";

import openConversationWith from "@actions/openConversationWith";
import colors from "@assets/colors";
import FirebaseAuthService from "@core/auth/FirebaseAuthService";
import DefaultCollabService from "@core/collab/DefaultCollabService";
import ChatKitMessageService from "@core/messages/ChatKitMessageService";
import MusicianService from "@core/musician/MusicianService";
import firebase from "@react-native-firebase/app";
import { goToAuthScreen, initializeLayouts } from "@screens/layouts";
import { MessagesStore, UserStore } from "@stores/index";
import { configure, when } from "mobx";
import React from "react";
import { Navigation } from "react-native-navigation";
import PushNotification from "react-native-push-notification";

console.disableYellowBox = true;

configure({ enforceActions: "observed" });

Navigation.setDefaultOptions({
  topBar: {
    visible: false,
    animate: false,
    elevation: 0,
    title: {
      color: colors.white
    },
    background: {
      color: colors.blacker
    },
    backButton: {
      color: colors.white
    }
  },
  layout: {
    componentBackgroundColor: colors.blacker,
    orientation: ["portrait"]
  },
  bottomTabs: {
    animate: false,
    backgroundColor: colors.black,
    titleDisplayMode: "alwaysShow"
  },
  bottomTab: {
    iconColor: colors.gray,
    textColor: colors.gray,
    selectedIconColor: colors.white,
    selectedTextColor: colors.white
  }
});

Navigation.events().registerAppLaunchedListener(async () => {
  const authService = new FirebaseAuthService();
  const musicianService = new MusicianService(authService);
  const collabService = new DefaultCollabService(authService);
  const messagesService = new ChatKitMessageService(authService);

  const userStore = new UserStore(authService, musicianService);
  const messagesStore = new MessagesStore(messagesService);

  await initializeLayouts(
    {
      userStore,
      messagesStore
    },
    {
      authService,
      collabService,
      messagesService
    }
  );

  await goToAuthScreen();

  let currentComponentId;

  Navigation.events().registerComponentDidAppearListener(event => {
    currentComponentId = event.componentId;
  });

  when(
    () => userStore.isAuthenticated,
    () => {
      PushNotification.configure({
        onRegister(token) {
          musicianService.registerDevice(token.token);
        },

        onNotification(notification) {
          if (currentComponentId === "messages.ConversationScreen") {
            return;
          }

          // @ts-ignore
          if (notification.roomId !== undefined && !notification.foreground) {
            const conversation = messagesStore.getConversation(
              // @ts-ignore
              notification.roomId
            );

            if (conversation !== undefined) {
              openConversationWith(
                currentComponentId,
                conversation.getRecipient()
              );
            }
          }
        },

        senderID: firebase.app().options.messagingSenderId
      });
    }
  );
});
