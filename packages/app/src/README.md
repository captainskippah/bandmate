# Folder Structure

`actions` - contains functions that can be invoked anywhere that mostly navigates to different screen when done. Examples: `logout` and `openConversation`.

`assets` - self-explanatory; contains stuff like images.

`components` - contains reusable components that are not specific to a certain screen.

`core` - contains classes and functions for business logic and are stateless. This is agnostic of UI state.

`models` - contains mobx's [Domain Objects](https://mobx.js.org/best/store.html).

`screens` - analogous to web pages. Should follow `package-by-page` approach which means each screen should be in a directory along with their screen-specific components. See [Routing](#routing--navigation).

`stores` - contains mobx's [Domain Stores](https://mobx.js.org/best/store.html). This is where most of the times core services are called and *should be the source of truth*.


# Routing / Navigation

[React Native Navigation](https://wix.github.io/react-native-navigation) is the library used for the routing because of the flexibility it gives. The declarative layout structuring is nice.

Each screen should be registered with their id as their location from `screens/` in dot notation (i.e `auth.LoginScreen`).
