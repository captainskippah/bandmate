import { StatefulService } from "@core/interface";

export function initialized(
  target: StatefulService,
  property,
  desc: PropertyDescriptor
) {
  if (typeof target.initialize === "undefined") {
    throw new Error(`${target.constructor.name} is not a StatefulService!`);
  }

  const originalFn = desc.value;

  desc.value = function(...args) {
    // @ts-ignore
    if (!this.isInitialized()) {
      throw new Error(`${target.constructor.name} is not initialized!`);
    }

    return originalFn.apply(this, args);
  };

  return desc;
}
