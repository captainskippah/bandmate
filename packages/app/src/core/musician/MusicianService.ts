import { Location, Musician } from "@bandmate/types/musician";
import AuthService from "@core/auth/AuthService";
import crashlytics from "@react-native-firebase/crashlytics";
import config from "react-native-config";

export default class MusicianService {
  private readonly authService: AuthService;

  constructor(authService: AuthService) {
    this.authService = authService;
  }

  public async getMusician(): Promise<Musician | undefined> {
    const token = await this.authService.getToken();

    const response = await fetch(`${config.API_URL}/musicians/current`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });

    if (response.status === 404) {
      return undefined;
    }

    if (response.status === 200) {
      return (await response.json()) as Musician;
    }

    if (!__DEV__) {
      crashlytics().log("MusicianService.getMusician failed");
      crashlytics().setAttributes({
        status: response.status.toString(),
        text: await response.text()
      });
    }

    throw new Error(await response.text());
  }

  public async updateMusician(musician: Musician): Promise<void> {
    const token = await this.authService.getToken();

    const response = await fetch(`${config.API_URL}/musicians/current`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(musician)
    });

    if (response.status !== 204) {
      if (!__DEV__) {
        crashlytics().log("MusicianService.updateMusician failed");
        crashlytics().setAttributes({
          musician: JSON.stringify(musician),
          status: response.status.toString(),
          text: await response.text()
        });
      }
      throw new Error(await response.text());
    }
  }

  public async updateLocation(location: Location): Promise<void> {
    const token = await this.authService.getToken();

    const response = await fetch(
      `${config.API_URL}/musicians/current/location`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(location)
      }
    );

    if (response.status !== 204) {
      if (!__DEV__) {
        crashlytics().log("MusicianService.updateLocation failed");
        crashlytics().setAttributes({
          location: JSON.stringify(location),
          status: response.status.toString(),
          text: await response.text()
        });
      }
      throw new Error(await response.text());
    }
  }

  public async registerDevice(token: string) {
    const authToken = await this.authService.getToken();

    const response = await fetch(
      `${config.API_URL}/musicians/current/devices`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${authToken}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ token })
      }
    );

    if (response.status !== 204) {
      if (!__DEV__) {
        crashlytics().log("MusicianService.registerDevice failed");
        crashlytics().setAttributes({
          token,
          status: response.status.toString(),
          text: await response.text()
        });
      }
      throw new Error(await response.text());
    }
  }
}
