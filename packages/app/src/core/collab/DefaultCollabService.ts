import { CollabFilter } from "@bandmate/types/collab";
import { Musician } from "@bandmate/types/musician";
import AuthService from "@core/auth/AuthService";
import CollabService from "@core/collab/CollabService";
import crashlytics from "@react-native-firebase/crashlytics";
import queryString from "query-string";
import config from "react-native-config";

export default class DefaultCollabService implements CollabService {
  private readonly authService: AuthService;

  constructor(authService: AuthService) {
    this.authService = authService;
  }

  public async get(filter: CollabFilter): Promise<Musician[]> {
    const response = await fetch(
      `${config.API_URL}/collab?${queryString.stringify(filter)}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${await this.authService.getToken()}`
        }
      }
    );

    if (response.status === 200) {
      return (await response.json()) as Musician[];
    }

    if (!__DEV__) {
      crashlytics().log("CollabService.get failed");
      // @ts-ignore
      await crashlytics().setAttributes({
        ...filter,
        status: response.status.toString(),
        text: await response.text()
      });
    }

    throw new Error(await response.text());
  }
}
