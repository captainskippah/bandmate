import { CollabFilter } from "@bandmate/types/collab";
import { Musician } from "@bandmate/types/musician";

export default interface CollabService {
  get(filter: CollabFilter): Promise<Musician[]>;
}
