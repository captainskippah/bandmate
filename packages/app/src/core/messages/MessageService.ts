import { StatefulService } from "@core/interface";
import { Message, MessageServiceListeners } from "./types";

export default interface MessageService extends StatefulService {
  receiveConversations(): Promise<void>;

  createConversation(
    conversationId: string,
    recipientId: string
  ): Promise<void>;

  getMessages(conversationId: string, initialId: string): Promise<Message[]>;

  sendMessage(conversationId: string, message: Message): Promise<void>;

  seen(conversationId: string, messageId: string): Promise<void>;

  getUserId(): string;

  registerListeners(listeners: MessageServiceListeners): void;
}
