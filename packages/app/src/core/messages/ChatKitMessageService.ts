import AuthService from "@core/auth/AuthService";
import { initialized } from "@core/decorators";
import MessageService from "@core/messages/MessageService";
import { Message, MessageServiceListeners } from "@core/messages/types";
import { ChatManager, TokenProvider } from "@pusher/chatkit-client/react-native";
import crashlytics from "@react-native-firebase/crashlytics";
import config from "react-native-config";

interface ChatKitUsers {
  id: string;
  name: string;
  avatarURL: string;
  presence: {
    state: string;
  };
}

interface ChatKitCursor {
  position: number;
  updatedAt: string;
  room: ChatKitRoom;
  user: ChatKitUsers;
  type: number;
}

interface FetchMultipartMessageParams {
  roomId: string;
  initialId?: number;
  limit?: number;
  direction?: "newer" | "older";
}

interface CreateRoomParams {
  id?: string;
  name: string;
  addUserIds: string[];
  private: boolean;
}

interface SubscribeToRoomMultipartParams {
  roomId: string;
  messageLimit?: number;
  hooks?: {
    onMessage(message: ChatKitMessage): void | Promise<void>;
  };
}

interface SendSimpleMessageParams {
  roomId: string;
  text: string;
}

interface ChatKitUser {
  rooms: ChatKitRoom[];
  users: ChatKitUsers[];

  readCursor({ roomId: string }): Promise<ChatKitCursor | undefined>;

  setReadCursor({ roomId: string, position: number }): Promise<void>

  fetchMultipartMessages(
    params: FetchMultipartMessageParams
  ): Promise<ChatKitMessage[]>;

  sendSimpleMessage(params: SendSimpleMessageParams): Promise<string>;

  createRoom(params: CreateRoomParams): Promise<ChatKitRoom>;

  subscribeToRoomMultipart(params: SubscribeToRoomMultipartParams): Promise<void>;
}

interface ChatKitRoom {
  id: string;
  isPrivate: boolean;
  name: string;
  users: ChatKitUsers[];
  unreadCount: number;
  lastMessageAt: string;
  customData: object;
}

interface ChatKitMessage {
  id: number;
  sender: ChatKitUsers;
  room: ChatKitRoom;
  parts: [
    {
      partType: string;
      payload: {
        content: string;
      };
    }
  ];
  createdAt: string;
  updatedAt: string;
}

export default class ChatKitMessageService implements MessageService {
  private chatManager?;
  private user?: ChatKitUser;
  private listeners?: MessageServiceListeners;

  private get userId() {
    return this.authService.getUser()?.id;
  }

  private readonly authService: AuthService;

  private readonly conversationMap: {
    [key: string]: string;
  };

  private readonly roomsBeingCreated: {
    [key: string]: Promise<void>;
  };

  private readonly messageMap: {
    [key: string]: string;
  };

  private readonly messageBeingSent: {
    [key: string]: Promise<void>;
  };

  constructor(authService: AuthService) {
    this.authService = authService;
    this.conversationMap = {};
    this.roomsBeingCreated = {};
    this.messageMap = {};
    this.messageBeingSent = {};
  }

  public async initialize() {
    if (!this.canInitialize()) {
      throw new Error("ChatKitMessageService: Cannot initialize without userId");
    }

    console.log("ChatKitMessageService: will now initialize");

    this.chatManager = new ChatManager({
      userId: this.userId,
      instanceLocator: config.CHATKIT_INSTANCE,
      tokenProvider: new TokenProvider({
        url: config.CHATKIT_TOKEN_URL
      })
    });

    this.user = await this.chatManager.connect({
      onAddedToRoom: async room => {
        await Promise.all(Object.values(this.roomsBeingCreated));

        // We created this room so we dont need to receive it
        if (this.conversationMap[room.id] !== undefined) {
          return;
        }

        // We have to subscribe early to be able to get the recipient
        await this.user!.subscribeToRoomMultipart({
          roomId: room.id
        });

        const recipient = this.getRecipient(room);

        this.listeners!.onConversationReceived(room.id, recipient);

        await this.attachMessageReceivedHandler(room.id);
      }
    });

    console.log("ChatKitMessageService: now initialized");
  }

  public canInitialize() {
    return this.userId !== undefined;
  }

  public isInitialized() {
    return this.chatManager !== undefined && this.user !== undefined;
  }

  public dispose() {
    if (this.chatManager !== undefined) {
      this.chatManager.disconnect();
    }
    this.listeners = undefined;
  }

  @initialized
  public async receiveConversations(): Promise<void> {
    const rooms = this.user!.rooms;

    await Promise.all(
      rooms.map(async room => {
        const [cursor, messages] = await Promise.all([
          this.user!.readCursor({
            roomId: room.id
          }),
          this.user!.fetchMultipartMessages({
            roomId: room.id,
            direction: "newer",
            limit: 1
          })
        ]);

        console.log(`ChatKitMessageService: Subscribing to room ${room.id} to get recipient`);

        // We have to subscribe early to be able to get the recipient
        await this.user!.subscribeToRoomMultipart({
          roomId: room.id
        });

        const recipient = this.getRecipient(room);

        // Intent not kind of clear here.
        // onConversationReceived was meant to be used for receiving from others.
        // In the future, we can probably create a separate
        // listener for receiving initial conversations if stuff gets messy.
        this.listeners!.onConversationReceived(room.id, recipient, {
          firstMessageId: messages[0]?.id.toString(),
          lastSeenMessageId: cursor?.position.toString()
        });

        console.log(`ChatKitMessageService: Subscribing again to room ${room.id} for message receive handler`);

        await this.attachMessageReceivedHandler(room.id);
      })
    );
  }

  @initialized
  public async getMessages(
    conversationId: string,
    initialId: string
  ): Promise<Message[]> {
    const messages: ChatKitMessage[] = await this.user!.fetchMultipartMessages({
      roomId: this.conversationMap[conversationId] || conversationId,
      initialId: Number(initialId)
    });

    return messages.map(message => ({
      id: message.id.toString(),
      message: message.parts[0].payload.content,
      sender: message.sender.id,
      date: new Date(message.createdAt)
    }));
  }

  @initialized
  public createConversation(
    conversationId: string,
    recipientId: string
  ): Promise<void> {
    this.roomsBeingCreated[conversationId] = (async () => {
      const room = await this.user!.createRoom({
        name: [this.userId!, recipientId]
          .sort((a, b) => a!.localeCompare(b!))
          .join("-"),
        private: true,
        addUserIds: [this.userId!, recipientId]
      });

      this.conversationMap[conversationId] = room.id;
      this.conversationMap[room.id] = conversationId;

      await this.attachMessageReceivedHandler(room.id);

      delete this.roomsBeingCreated[conversationId];
    })();

    return this.roomsBeingCreated[conversationId];
  }

  @initialized
  public async sendMessage(
    conversationId: string,
    message: Message
  ): Promise<void> {
    this.messageBeingSent[message.id] = (async () => {
      const subscription = this.roomsBeingCreated[conversationId];

      // Wait to be subscribed to room.
      // We can not send message if not subscribed to room
      if (subscription !== undefined) {
        await subscription;
      }

      // If we don't have the value in Map, it means this conversation
      // was loaded from the server. Only Conversations created from our app
      // has a custom id.
      const roomId = this.conversationMap[conversationId] || conversationId;

      const response = await fetch(
        `${config.API_URL}/rooms/${roomId}/messages`,
        {
          method: "POST",
          headers: {
            "Authorization": `Bearer ${await this.authService.getToken()}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            text: message.message
          })
        });

      if (response.status === 201) {
        const chatkitMessageId = (await response.json()).message_id;
        this.messageMap[chatkitMessageId] = message.id;
        this.messageMap[message.id] = chatkitMessageId;
      } else {
        if (!__DEV__) {
          crashlytics().log("ChatKitMessageService.sendMessage failed.");
          crashlytics().setAttributes({
            status: response.status.toString(),
            text: await response.text()
          })
        }
      }

      delete this.messageBeingSent[message.id];
    })();
  }

  public seen(conversationId: string, messageId: string) {
    return this.user!.setReadCursor({
      roomId: conversationId,
      position: Number(this.messageMap[messageId] || messageId)
    });
  }

  public getUserId(): string {
    return this.userId!;
  }

  public registerListeners(listeners: MessageServiceListeners) {
    this.listeners = listeners;
  }

  private async attachMessageReceivedHandler(roomId: string) {
    await this.user!.subscribeToRoomMultipart({
      roomId,
      hooks: {
        onMessage: async (message: ChatKitMessage) => {
          // Wait for all messages to be resolved
          // because this received message might be one of them
          await Promise.all(Object.values(this.messageBeingSent));

          // Received message is the one that we sent
          if (this.messageMap.hasOwnProperty(message.id)) {
            return;
          }

          await Promise.all(Object.values(this.roomsBeingCreated));

          // If id is not found in messageMap it means
          // we are receiving. No mapping is needed.
          const id = this.messageMap[message.id] || message.id.toString();

          // If id is not found in conversationMap it means
          // we are receiving new conversation. No mapping is needed.
          const conversationId =
            this.conversationMap[message.room.id] || message.room.id;

          if (this.listeners === undefined) {
            console.warn("Message received but no Listeners are set");
            return;
          }

          this.listeners.onMessageReceived(conversationId, {
            id,
            message: message.parts[0].payload.content,
            sender: message.sender.id,
            date: new Date(message.createdAt)
          });
        }
      }
    });
  }

  private getRecipient(room: ChatKitRoom) {
    // Usually recipient is index 1
    let recipient = {
      id: room.users[1].id,
      name: room.users[1].name,
      image: room.users[1].avatarURL
    };

    // But this might happen so just to be sure
    if (recipient.id === this.userId) {
      recipient = {
        id: room.users[0].id,
        name: room.users[0].name,
        image: room.users[0].avatarURL
      };
    }

    return recipient;
  }
}
