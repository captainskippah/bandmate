export interface ConversationMeta {
  firstMessageId?: string;
  lastSeenMessageId?: string;
}

export interface MessageServiceListeners {
  onConversationReceived(
    conversationId: string,
    recipient: User,
    meta?: ConversationMeta
  ): void;

  onMessageReceived(conversationId: string, message: Message): void;
}

export interface User {
  id: string;
  name: string;
  image: string;
}

export interface Message {
  id: string;
  message: string;
  sender: string;
  date: Date;
}

export interface Conversation {
  id: string;
  messages: Message[];
  recipients: User[];
}
