import { AuthUser } from "@core/auth/types";
import { StatefulService } from "@core/interface";

export default interface AuthService extends StatefulService {
  facebookLogin(): Promise<boolean>;
  logout(): Promise<void>;
  getUser(): AuthUser | undefined;
  getToken(): Promise<string>;
  isAuthenticated(): boolean;
}
