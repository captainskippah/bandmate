export interface AuthUser {
  id: string;
  name: string;
  image: string;
}
