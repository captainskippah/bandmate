import AuthService from "@core/auth/AuthService";
import { AuthUser } from "@core/auth/types";
import { initialized } from "@core/decorators";
import auth from "@react-native-firebase/auth";
import crashlytics from "@react-native-firebase/crashlytics";
import storage from "@react-native-firebase/storage";
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager
} from "react-native-fbsdk";

export default class FirebaseAuthService implements AuthService {
  private initialized: boolean;
  private authenticated: boolean;

  constructor() {
    this.initialized = false;
    this.authenticated = false;
  }

  public isInitialized(): boolean {
    return this.initialized;
  }

  public async initialize(): Promise<void> {
    if (!this.initialized) {
      console.log("FirebaseAuthService: will now initialize.");

      const user = auth().currentUser;

      console.log({ user });

      if (user !== null) {
        crashlytics().setUserId(user.uid);
        try {
          await user!.reload();
        } catch (e) {
          if (e.code === "auth/invalid-user-token") {
            await user.getIdToken(true);
            await this.initialize();
          }

          if (__DEV__) {
            console.error("FirebaseAuthService: initialization failed.", e);
          } else {
            crashlytics().log("FirebaseAuthService: initialization failed.");
            crashlytics().recordError(e);
          }

          throw e;
        }
      }

      this.initialized = true;

      console.log("FirebaseAuthService: now initialized.");
    }
  }

  public dispose(): void {
    console.log("FirebaseAuthService: disposing.");
    this.initialized = false;
    console.log("FirebaseAuthService: disposed.");
  }

  public isAuthenticated(): boolean {
    return auth().currentUser !== null;
  }

  @initialized
  public async facebookLogin(): Promise<boolean> {
    const fbAuth = await LoginManager.logInWithPermissions(["public_profile"]);

    if (fbAuth.isCancelled) {
      return false;
    }

    const fbToken = await AccessToken.getCurrentAccessToken();

    if (fbToken === null) {
      throw new Error(
        "Something went wrong obtaining the user access token from Facebook"
      );
    }

    const fbCredential = auth.FacebookAuthProvider.credential(
      fbToken.accessToken
    );

    const firebaseCredential = await auth().signInWithCredential(fbCredential);

    // Check if we have uploaded custom profile picture
    // If not, we get large pic from facebook
    await storage()
      .ref("users")
      .child(firebaseCredential.user.uid)
      .child("image")
      .getMetadata()
      .catch(async error => {
        if (error.code !== "storage/object-not-found") {
          if (__DEV__) {
            console.error(error);
          } else {
            crashlytics().log(
              "FirebaseAuthService: unexpected error while fetching profile picture from firebase storage."
            );
            crashlytics().recordError(error);
          }
        } else {
          await new Promise(resolve => {
            const graphRequest = new GraphRequest(
              "/me",
              {
                accessToken: fbToken.accessToken,
                parameters: {
                  fields: {
                    string: "picture.type(large)"
                  }
                }
              },
              async (error, result) => {
                if (error) {
                  if (__DEV__) {
                    console.error(error);
                  } else {
                    crashlytics().log(error.toString());
                  }
                }

                // @ts-ignore
                const photoURL = result!.picture.data.url;
                await firebaseCredential.user.updateProfile({ photoURL });
                resolve();
              }
            );

            new GraphRequestManager().addRequest(graphRequest).start();
          });
        }
      });

    return true;
  }

  @initialized
  public getUser(): AuthUser | undefined {
    const user = auth().currentUser;

    if (user === null) {
      return undefined;
    }

    return {
      id: user.uid,
      image: user.photoURL!,
      name: user.displayName!
    };
  }

  @initialized
  public getToken(): Promise<string> {
    return auth().currentUser!.getIdToken();
  }

  @initialized
  public async logout(): Promise<void> {
    await auth().signOut();
  }
}
