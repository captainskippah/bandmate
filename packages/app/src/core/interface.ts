export interface StatefulService {
  initialize(): Promise<void>;
  isInitialized(): boolean;
  dispose(): void;
}
